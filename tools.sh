#!/bin/bash
VENV_NAME="sport"
PIP_REQ="requirements.txt"

if [[ $# -eq 0 ]]; then
    echo "No arguments given"
    echo "Usage: sh tools.sh (create)|(install)|(hooks)"
    exit
fi

#for DEC-10 PC
module add python/3.4.3

create_venv() {
    # create virtual environment if it doesn't exist
    if [[ ! -d "$VENV_NAME" ]]; then
        #virtualenv $VENV_NAME
        echo "Creating virtual environment \"$VENV_NAME\""
        python -m venv $VENV_NAME && echo "Finished"
    else
        echo "Virtual environment \"$VENV_NAME\" already exists in this directory"
    fi
}

activate_venv() {
    #activate the virtual environment if it exists
    if [[ -e $VENV_NAME/bin/activate ]]; then
        source $VENV_NAME/bin/activate
        echo "Activated virtual environment"
    else
        echo "Could not activate virtual environment \"$VENV_NAME\", it doesn't exist"
        echo "Create with sh tools.sh create"
        exit
    fi
}

if [[ "$1" == "create" ]]; then
    create_venv
    exit
fi

if [[ "$1" == "install" ]]; then
    activate_venv

    #determine whether in a virtual environment
    [[ "$VIRTUAL_ENV" == "" ]]; INVENV=$?

    #exit if pip requirements file doesn't exist
    [[ -e $PIP_REQ ]] || (echo "\"$PIP_REQ\" does not exist" && exit)

    #if in virtual environment and requirements is readable then install them
    if [[ $INVENV -eq 1 ]] && [[ -r $PIP_REQ ]]; then
        echo "Installing modules from \"$PIP_REQ\""
        $VENV_NAME/bin/pip install -r $PIP_REQ
    fi
fi

if [[ "$1" == "hooks" ]]; then
    activate_venv
    HOOK_PATH=".git/hooks/pre-commit"
    if [[ -e $HOOK_PATH ]]; then
        echo "deleting old hook";
        rm $HOOK_PATH;
    fi
    HOOK=$'#!/bin/bash
source $( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/../../sport/bin/activate
for i in $(git diff --name-only --cached); do
    if [[ $i =~ \.py$ ]]; then
        black --exclude "/(\.git|sport)/" $( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/../../$i
        git add $( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/../../$i
    fi
done'
    echo "$HOOK" >> $HOOK_PATH;
    echo "created new hook";
    chmod +x $HOOK_PATH;
    exit;
fi
