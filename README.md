### How to run the code
1. Clone the repository
2. In the root of the repository create a python 3 virtual environment `python3 -m venv sport`
3. Activate the virtual environment `source sport/bin/activate`
4. Install the requirements `pip install -r requirements.txt` (if the virtual environment is not version 3.6+ then the module *black* will fail to install. If this is the case please remove this from requirements.txt and re-run the command. The module *black* is only needed for development purposes to format the code)
5. Move into the app directory `cd app`
6. Initialize the flask database `flask db init && flask db migrate && flask db upgrade`
7. Populate the database with sample data `python populate_db.py`
8. Run the application `python run.py`
9. Open http://localhost:5000