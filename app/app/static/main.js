$(document).ready(function () {



    var csrf_token = $("meta[name=csrf-token]").attr("content");
    // Configure ajaxSetupso that the CSRF token is added to the header of every request
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (
                !/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) &&
                !this.crossDomain
            ) {
                xhr.setRequestHeader("X-CSRFToken", csrf_token);
            }
        }
    });

    // -------------------------------------- client side validation -----------------------------------

    // documentation availabel at: https://github.com/bnabriss/jquery-form-validation
    $(document).on('keyup blur', 'input[data-validator]', function () {
        new Validator($(this));
    });

    // handle sumbiting the form
    $(document).on('submit', '.formContainer', function (e) {
        let elem = $(this).find('.form-group.has-error'); //.is("[has-error]");
        console.log(elem);
        if (elem.length > 0) {
            e.stopImmediatePropagation();
            e.preventDefault();

        }
    });


    // ----------------------------- dynamic modal helpers -------------------------------------

    //removing ajax content from the edit recipe modal on hide
    $('#dynamicModal').on('hidden.bs.modal', function (e) {
        $(this).find("#dynamicModalInsert").empty();
    })


    // ----------------------------- handle ajax login form  ----------------------------------

    $(document).on("click", "#showLoginForm", function (e) {
        let url = $(this).attr("data");
        $.ajax({
            url: url,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $("#dynamicModalInsert")
                    .append(response.data);
                //$("#loginForm").validator();
            },
            error: function (error) {
                console.log(error);
            }
        });
    });



    $(document).on("submit", "#loginForm", function (e) {
        $.ajax({
            url: $(this).attr("action"),
            type: "POST",
            data: $(this).serialize(),
            success: function (response) {
                if (response.status == "OK") {
                    $("#dynamicModal").remove();
                    $(".modal-backdrop").remove();
                    $("#main").prepend(response.flash);
                    $("#navbar").replaceWith(response.navbar);
                    if (response.redirect) {
                        window.location.replace(response.redirect);
                    }
                } else if (response.status == "BAD") {
                    $("#dynamicModalInsert")
                        .empty();
                    $("#dynamicModalInsert")
                        .append(response.data);
                } else if (response.status == "MEDIUM") {
                    $("#dynamicModal").remove();
                    $(".modal-backdrop").remove();
                    if (response.redirect) {
                        window.location.replace(response.redirect);
                    }
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
        e.preventDefault();
    });

    // ----------------------------- trigger popup on auth required  ----------------------------------

    const isAuthRequired = () => {
        if ($.cookie("auth") == 'required') {
            $("#showLoginForm").click();
            $.removeCookie('auth', {
                path: '/'
            });
        }
    }

    isAuthRequired();

})