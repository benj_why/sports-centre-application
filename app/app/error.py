from flask import Blueprint, render_template
from flask import current_app as app
from app import db, bcrypt

errors_bp = Blueprint("errors", __name__)


@errors_bp.app_errorhandler(404)
def not_found_error(error):
    return render_template("errors/404.html", title="404"), 404


@errors_bp.app_errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template("errors/500.html", title="500"), 500
