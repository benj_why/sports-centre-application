from flask import (
    Blueprint,
    render_template,
    jsonify,
    request,
    redirect,
    url_for,
    abort,
    session,
    flash,
)
from flask import current_app as app
from flask_login import current_user, login_user, logout_user, login_required

# need to change these the not camel casing
from app.user.forms import (
    Signup,
    LoginForm,
    EditDetails,
    confirmPasswordForm,
    changePasswordForm,
    CancelBookingForm,
    VerifyForm,
)
from app import db, bcrypt
from app.models import User, Activity, Booking, PaymentLog, IPLog

from flask import current_app as app

from sqlalchemy import extract, and_
from datetime import datetime

from app.email import send_email
from email.mime.text import MIMEText

import random
import json
import re
from urllib.request import urlopen
import hashlib
from app.main.helpers import get_activity_price

try:
    from flask_weasyprint import render_pdf, HTML
except:
    pass

# from app.main.forms import ()

# Blueprint Configuration
user_bp = Blueprint(
    "user_bp",
    __name__,
    template_folder="templates",
    static_folder="static",
    url_prefix="/user",
)


@user_bp.route("/", methods=["GET"])
def home():
    """Homepage route."""
    return render_template("user.html")


# ------------------------------sign up ---------------------------------------


@user_bp.route("/privacyPolicy", methods=["GET"])
def privacy_policy():
    return render_template("privacy_policy.html")


@user_bp.route("/signup", methods=["GET", "POST"])
def signup():
    signup_form = Signup()
    if signup_form.validate_on_submit():
        create_user(
            signup_form.first_name.data,
            signup_form.last_name.data,
            signup_form.email.data,
            signup_form.password.data,
            signup_form.emergency_number.data,
            signup_form.dob.data,
        )

        return redirect(url_for("user_bp.userAccount"))

    for fieldName, errorMessages in signup_form.errors.items():
        for err in errorMessages:
            print(err)

    return render_template("signup.html", form=signup_form)


def create_user(first_name, last_name, email, password, emergency_num, dob):
    # create new user, hash password and add to the database
    password_hash = bcrypt.generate_password_hash(password).decode("utf-8")
    new_user = User(
        first_name=first_name,
        last_name=last_name,
        email=email,
        password=password_hash,
        emergency_number=emergency_num,
        dob=dob,
        user_type=3,
    )
    db.session.add(new_user)
    db.session.commit()
    login_user(new_user)


# --------------------------login --------------------------------------------
@user_bp.route("/validateLogin", methods=["GET", "POST"])
def validate_login():
    verify = VerifyForm()
    if verify.validate_on_submit():
        code_received = datetime.now()
        diff = code_received - session["code_sent"]
        min_diff = diff.seconds // 60
        session.pop("code_sent")
        if (
            hashlib.md5(verify.code.data.encode("utf-8")).hexdigest()
            == session["verify_code"]
            and min_diff <= 5
        ):
            user = User.query.get(session["login_user"])
            new_log = IPLog(
                user_id=user.id,
                ip_addr=hashlib.md5(request.remote_addr.encode("utf-8")).hexdigest(),
            )
            db.session.add(new_log)
            db.session.commit()
            session.pop("login_user")
            session.pop("verify_code")
            login_user(user)
            return redirect(url_for("user_bp.home"))
        else:
            session.pop("login_user")
            session.pop("verify_code")
            # flash error message
            return redirect(url_for("main_bp.index"))

    else:
        uid, name, email = current_user.id, current_user.first_name, current_user.email
        ip_addr = request.remote_addr
        log = IPLog.query.get((uid, hashlib.md5(ip_addr.encode("utf-8")).hexdigest()))
        if log != None:
            return redirect(url_for("user_bp.home"))
        else:
            logout_user()
            code = random.randint(100000, 999999)
            print(code)
            session["verify_code"] = hashlib.md5(str(code).encode("utf-8")).hexdigest()
            session["login_user"] = uid
            session["code_sent"] = datetime.now()
            if app.debug:
                print("Code: {}".format(code))

            response = urlopen("http://ipinfo.io/json")
            data = json.load(response)
            city = data["city"]
            country = data["country"]
            send_email(
                "Edgier Attempted Login",
                email,
                MIMEText(
                    render_template("new_login_email.html", code=code, name=name),
                    "html",
                ),
            )
            return render_template(
                "new_login.html",
                verify_form=verify,
                email=email,
                ip_addr=ip_addr,
                city=city,
                country=country,
            )


# login route - returns a form in json response
@user_bp.route("/login", methods=["GET", "POST"])
def login():

    # initialise login form
    login_form = LoginForm()

    # handle login request
    if request.method == "POST":
        if login_form.validate_on_submit():
            # if valid log the user in
            user = User.query.filter_by(email=login_form.email.data).first()
            login_user(user, remember=login_form.remember_me.data)
            # if the user is not a customer redirect them to the administrative home
            if user.user_type != 3:
                return jsonify({"status": "OK", "redirect": url_for("admin_bp.index")})
            else:
                log = IPLog.query.get((user.id, request.remote_addr))
                if log != None:
                    # if valid return json response and flash message
                    return jsonify(
                        {
                            "status": "OK",
                            "flash": render_template(
                                "flash.html",
                                message="You have successfully logged in",
                                category="success",
                            ),
                            "navbar": render_template("nav.html"),
                        }
                    )
                else:
                    return jsonify(
                        {
                            "status": "MEDIUM",
                            "redirect": url_for("user_bp.validate_login"),
                        }
                    )

        # if invalid return a login form with errors as json object
        else:
            return jsonify(
                {
                    "status": "BAD",
                    "data": render_template("loginForm.html", login_form=login_form),
                }
            )
    # if get request return the login form as json object
    else:
        return jsonify(
            {
                "status": "OK",
                "data": render_template("loginForm.html", login_form=login_form),
            }
        )


# ---------------------------------------------- logout -----------------------------

# registers the user as logged out through the flask login manager
@user_bp.route("/logout")
@login_required
def logout():
    logout_user()
    # redirect the user to the home page
    return redirect(url_for("main_bp.index"))


# -------------------------- user account page --------------------------------------------

# get the users account and handle user requests to chanege their details
@user_bp.route("/account", methods=["GET", "POST"])
@login_required
def userAccount():
    # generate edit user details form
    edit_user_form = EditDetails(current_user)
    if edit_user_form.validate_on_submit():
        # if valid then update the userr details
        current_user.first_name = edit_user_form.first_name.data
        current_user.last_name = edit_user_form.last_name.data
        current_user.email = edit_user_form.email.data
        current_user.emergency_number = edit_user_form.emergency_number.data
        current_user.dob = edit_user_form.dob.data
        # save the changes
        db.session.commit()
    # render the users account page this may contain form errors

    # get activities signed up for
    activities = Booking.query.filter_by(user_id=current_user.id).all()
    return render_template(
        "userAccount.html",
        bookings=current_user.bookings,
        edit_user_form=edit_user_form,
        activities=activities,
    )


# ---------------------------- Confirm Password -----------------------------------------

# route to render and handle the user the confirm password form
@user_bp.route("/confirmPassword", methods=["GET", "POST"])
@login_required
def confirmPassword():
    # create an instance of the form
    confirm_password_form = confirmPasswordForm(current_user)

    # if post check for form validation
    if request.method == "POST":
        if confirm_password_form.validate_on_submit():
            # if the password was correct return OK
            return jsonify({"status": "OK"})
        else:
            # if the password was incorrect return the confirm password form with errors as json
            return jsonify(
                {
                    "status": "BAD",
                    "data": render_template(
                        "confirmPasswordForm.html",
                        confirm_password_form=confirm_password_form,
                    ),
                }
            )
    # if a get request return the confirm password form as a json object
    return jsonify(
        {
            "status": "OK",
            "data": render_template(
                "confirmPasswordForm.html", confirm_password_form=confirm_password_form
            ),
        }
    )


# ---------------------------- Change password -----------------------------------------

# route to handle and render the change password form
@user_bp.route("/changePassword", methods=["GET", "POST"])
@login_required
def changePassword():
    # gcreate an instance of the change password form
    change_password_form = changePasswordForm(current_user)

    # if a POST request check for form validation
    if request.method == "POST":
        if change_password_form.validate_on_submit():
            # if the form is valid change the users password to their chosen new password
            current_user.set_password(change_password_form.new_password.data)
            db.session.commit()
            # return OK and a flash html message as a json object
            return jsonify(
                {
                    "status": "OK",
                    "flash": render_template(
                        "flash.html",
                        message="You have successfully changed your password",
                        category="success",
                    ),
                }
            )
        else:
            # if the users form was not valid then return the form with errors as a json object
            return jsonify(
                {
                    "status": "BAD",
                    "data": render_template(
                        "changePasswordForm.html",
                        change_password_form=change_password_form,
                    ),
                }
            )

    # if a get request return the change passwrod form as json object
    return jsonify(
        {
            "status": "OK",
            "data": render_template(
                "changePasswordForm.html", change_password_form=change_password_form
            ),
        }
    )


# ---------------------------- RECEIPT and BOOKING -----------------------------------------

# adds a booking to the booking table for a user & activity
def create_booking(user_id, activity_id):
    booking = Booking(
        user_id=user_id, activity_id=activity_id, time_of_booking=datetime.now()
    )
    db.session.add(booking)
    db.session.commit()


# sends a booking confirmation email to user booked onto an activity
def booking_email_confirmation(booking):
    subject = "Edgier Confirmation {}".format(booking.id)
    to_address = booking.user.email
    html_body = MIMEText(get_receipt(booking, "email"), "html")
    send_email(subject, to_address, html_body)


# returns a receipt given a user_id or activity_id
# content_type either returns receipt as HTML to render page / send as email
# or as a PDF to download
def get_receipt(booking, content_type):
    price, price_text, peak_time = get_activity_price(booking.activity)
    html = render_template(
        "receipt.html", booking=booking, price=price, price_text=price_text
    )
    if content_type == "html":
        return html
    if content_type == "pdf":
        return render_pdf(HTML(string=html))
    if content_type == "email":
        return re.sub("<nav.*?>.*?</nav>", "", html, flags=re.DOTALL)


# returns a list of a users bookings
@user_bp.route("/bookings", methods=["GET"])
@login_required
def bookings():
    bookings = []
    bookings = Booking.query.filter_by(user_id=current_user.id).all()

    return render_template("bookings.html", bookings=bookings)


# returns a list of a users payment history
@user_bp.route("/payments", methods=["GET"])
@login_required
def payments():
    payments = current_user.payments

    return render_template("payments.html", payments=payments)


# returns a list of a users payment history
@user_bp.route("/payments/<payment_id>", methods=["GET"])
@login_required
def payment(payment_id):
    payment = PaymentLog.query.filter_by(id=payment_id).first()
    if not payment:
        return abort(404)

    if payment.user_id != current_user.get_id():
        return abort(403)

    return render_template("payment.html", payment=payment)


# displays the payment receipt
@user_bp.route("/payments/<payment_id>/receipt", methods=["GET"])
@login_required
def payment_receipt(payment_id):
    payment = PaymentLog.query.filter_by(id=payment_id).first()
    if not payment:
        return abort(404)

    if payment.user_id != current_user.get_id():
        return abort(403)

    return render_template("payment_receipt.html", payment=payment)


# returns a page for an individual booking
@user_bp.route("/bookings/<booking_id>", methods=["GET", "POST"])
@login_required
def booking(booking_id):
    booking = Booking.query.filter_by(id=booking_id).first()
    if not booking:
        return abort(404)

    if booking.user_id != current_user.get_id():
        return abort(403)

    cancel_form = CancelBookingForm()
    if cancel_form.validate_on_submit():
        booking.cancel()

        # TODO: flash cancellation message
        return redirect(url_for("user_bp.bookings"))
    price, price_text, peak_time = get_activity_price(booking.activity)
    return render_template(
        "booking.html",
        booking=booking,
        cancel_form=cancel_form,
        price=price,
        price_text=price_text,
    )


@user_bp.route("/bookings/<booking_id>/receipt", methods=["GET", "POST"])
@login_required
def booking_receipt(booking_id):
    booking = Booking.query.filter_by(id=booking_id).first()
    if not booking:
        return abort(404)

    if booking.user_id != current_user.get_id():
        return abort(403)

    return get_receipt(booking, "html")


@user_bp.route("/bookings/<booking_id>/receipt_pdf", methods=["GET", "POST"])
@login_required
def booking_receipt_pdf(booking_id):
    booking = Booking.query.filter_by(id=booking_id).first()
    if not booking:
        return abort(404)

    if booking.user_id != current_user.get_id():
        return abort(403)

    return get_receipt(booking, "pdf")


@user_bp.route("bookings/<booking_id>/email_receipt", methods=["GET"])
@login_required
def email_receipt(booking_id):
    booking = Booking.query.get(booking_id)
    if not booking:
        return abort(404)

    if booking.user_id != current_user.get_id():
        return abort(403)

    booking_email_confirmation(booking)
    return redirect(request.referrer)
