$(document).ready(function () {

    var csrf_token = $("meta[name=csrf-token]").attr("content");
    // Configure ajaxSetupso that the CSRF token is added to the header of every request
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (
                !/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) &&
                !this.crossDomain
            ) {
                xhr.setRequestHeader("X-CSRFToken", csrf_token);
            }
        }
    });


    // ----------------------------- render login form  ----------------------------------

    $(".switchEditUserDetails").click(() => {
        $(".userDetails").toggle();
    });

    const toggleEditUserDetails = () => {
        $(".userDetails").toggle();
    }

    $("#hideEditUserDetails").hide();
    $(".toast").toast('show');


    $(document).on("click", "#getConfirmPassword", function () {
        let nextForm = $(this).attr("next");
        $.ajax({
            url: $(this).attr("data"),
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.status == "OK") {
                    $("#dynamicModalInsert").append(response.data);
                    $("#confirmPasswordForm").attr("next", nextForm);
                } else if (response.status == "BAD") {
                    console.log("bad error");
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    })

    $(document).on("submit", "#confirmPasswordForm", function (e) {
        let nextForm = $(this).attr("next");
        $.ajax({
            url: $(this).attr("action"),
            type: "POST",
            data: $(this).serialize(),
            success: function (response) {
                if (response.status == "OK") {
                    $("#dynamicModal").toggle();
                    $("#dynamicModal").click();
                    $(nextForm).submit();
                } else if (response.status == "BAD") {
                    console.log("incorrect password");
                    $("#dynamicModalInsert")
                        .empty();
                    $("#dynamicModalInsert")
                        .append(response.data);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
        e.preventDefault();
    })


    $(document).on("click", "#getChangePassword", function () {
        console.log("clicked");
        $.ajax({
            url: $(this).attr("data"),
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.status == "OK") {
                    $("#dynamicModalInsert").append(response.data);
                } else if (response.status == "BAD") {
                    console.log("bad error");
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    })

    $(document).on("submit", "#changePasswordForm", function (e) {
        $.ajax({
            url: $(this).attr("action"),
            type: "POST",
            data: $(this).serialize(),
            success: function (response) {
                if (response.status == "OK") {
                    $("#dynamicModal").toggle();
                    $("#dynamicModal").click();
                    $("#main").prepend(response.flash);
                } else if (response.status == "BAD") {
                    console.log("incorrect password");
                    $("#dynamicModalInsert")
                        .empty();
                    $("#dynamicModalInsert")
                        .append(response.data);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
        e.preventDefault();
    })

})