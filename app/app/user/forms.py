from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired, Length, Email, ValidationError, EqualTo
from wtforms.fields.html5 import DateField, EmailField

from app.models import User


class LoginForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Email()])
    password = PasswordField("Password", validators=[DataRequired()])
    remember_me = BooleanField("Remember Me")
    submit = SubmitField("Sign In")

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is None:
            raise ValidationError("Email is incorrect.")

    def validate_password(self, password):
        user = User.query.filter_by(email=self.email.data).first()
        if user:
            if not user.check_password(password.data):
                raise ValidationError("Password is incorrect.")


class Signup(FlaskForm):
    first_name = StringField("First Name", validators=[DataRequired()])
    last_name = StringField("Last Name", validators=[DataRequired()])
    email = EmailField("Email", validators=[DataRequired(), Email()])
    password = PasswordField("Password", validators=[DataRequired(), Length(6, 16)])
    emergency_number = StringField(
        "Emergency Phone Number", validators=[DataRequired(), Length(11, 11)]
    )
    dob = DateField("Date of Birth", validators=[DataRequired()])

    def validate_email(self, email):
        exists = len(User.query.filter_by(email=email.data).all()) != 0
        if exists:
            raise ValidationError("Email address already taken.")

    def validate_emergency_number(self, number):
        for digit in number.data:
            if not digit.isdigit():
                raise ValidationError("Phone number must only contain numeric digits.")


class EditDetails(FlaskForm):
    first_name = StringField("First Name", validators=[DataRequired()])
    last_name = StringField("Last Name", validators=[DataRequired()])
    email = EmailField("Email", validators=[DataRequired(), Email()])
    emergency_number = StringField(
        "Emergency Phone Number", validators=[DataRequired(), Length(11, 11)]
    )
    dob = DateField("Date of Birth", validators=[DataRequired()])

    def __init__(self, current_user):
        super().__init__()
        if not self.dob.data:
            self.dob.data = current_user.dob


class confirmPasswordForm(FlaskForm):

    password = StringField("Password", validators=[DataRequired()])
    submit = SubmitField("Confirm Password")
    user = None

    def __init__(self, current_user):
        super().__init__()
        self.user = current_user

    def validate_password(self, password):
        if not self.user.check_password(password.data):
            raise ValidationError("The password you entered was incorrect")


class changePasswordForm(FlaskForm):

    password = StringField("Password", validators=[DataRequired()])
    new_password = StringField("New Password", validators=[DataRequired()])
    new_password_confirm = StringField(
        "Confirm new Password", validators=[DataRequired(), EqualTo("new_password")]
    )
    submit = SubmitField("Change Password")
    user = None

    def __init__(self, current_user):
        super().__init__()
        self.user = current_user

    def validate_password(self, password):
        if not self.user.check_password(password.data):
            raise ValidationError("The password you entered was incorrect")

    def validate_new_password(self, new_password):
        if not self.password != new_password:
            raise ValidationError(
                "Your new password must be different from your previous password"
            )

    def validate_new_password_confirm(self, new_password_confirm):
        if not self.new_password != new_password_confirm:
            raise ValidationError("Your passwords do not match")


class CancelBookingForm(FlaskForm):
    submit = SubmitField("Cancel Booking", validators=[DataRequired()])


class VerifyForm(FlaskForm):
    code = StringField("code", validators=[DataRequired()])
