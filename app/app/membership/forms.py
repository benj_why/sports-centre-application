from flask_wtf import FlaskForm
from wtforms import (
    RadioField,
    IntegerField,
    DateField,
    TextField,
    SubmitField,
    BooleanField,
)
from wtforms.validators import DataRequired, ValidationError, Regexp

# Length validator for user input strings
class Length(object):
    def __init__(self, min=-1, max=-1, message=None):
        self.min = min
        self.max = max
        if not message:
            message = u"Field must be between %i and %i characters long." % (min, max)
        self.message = message

    def __call__(self, form, field):
        l = field.data and len(field.data) or 0
        if l < self.min or self.max != -1 and l > self.max:
            raise ValidationError(self.message)


length = Length

class BillingOptionForm(FlaskForm):
    monthly = SubmitField(label="Pay Monthly")
    annually = SubmitField(label="Pay Annually")

class ManageForm(FlaskForm):
    cancel = SubmitField(label="Cancel Membership")
    renew_monthly = SubmitField(label="Renew Monthly")
    renew_annually = SubmitField(label="Renew Annually")

class JoinMemberForm(FlaskForm):
    billing_option = RadioField(
        "Billing Option",
        choices=[("m", "Monthly"), ("y", "Annually")],
        validators=[DataRequired()],
    )
    card_number = TextField(
        "Card Number",
        validators=[
            DataRequired(),
            Regexp(
                "^(?:4[0-9]{12}(?:[0-9]{3})?|  (?:5[1-5][0-9]{2}| 222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}|3[47][0-9]{13}|  3(?:0[0-5]|[68][0-9])[0-9]{11}|6(?:011|5[0-9]{2})[0-9]{12}|(?:2131|1800|35\d{3})\d{11})$"
            ),
        ],
    )
    expiry_date = DateField("Expiry Date", format="%m/%y", validators=[DataRequired()])
    name_on_card = TextField(
        "Name on Card",
        validators=[
            DataRequired(),
            length(1, 30, "Name must be fewer than 30 characters long"),
        ],
    )
    address_1 = TextField(
        "Address Line 1",
        validators=[
            DataRequired(),
            length(1, 50, "Address must be fewer than 50 characters long"),
        ],
    )
    postcode = TextField(
        "Postcode",
        validators=[
            DataRequired(),
            Regexp(
                "([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9][A-Za-z]?))))\s?[0-9][A-Za-z]{2})"
            ),
        ],
    )
    security_code = TextField(
        "Security Code", validators=[DataRequired(), Regexp("^[0-9]{3,4}$")]
    )
    save_card = BooleanField("Save Card Details")
    submit = SubmitField("Submit", validators=[DataRequired()])
