from flask import Blueprint, render_template, flash, redirect, request, session, url_for, abort
from flask import current_app as app
from flask_login import current_user, login_user, logout_user, login_required

# from app.main.forms import ()
from .forms import BillingOptionForm, ManageForm
from app.models import Membership, Card, MembershipRenewal
from datetime import date, timedelta
import calendar
from app import db
from app.models import User
from app.pay.payments import create_pending_payment
from .helpers import get_prices

from app.email import send_email
from email.mime.text import MIMEText

# Blueprint Configuration
membership_bp = Blueprint(
    "membership_bp",
    __name__,
    template_folder="templates",
    static_folder="static",
    url_prefix="/membership",
)



@membership_bp.route("/manage", methods=["GET", "POST"])
@login_required
def membership_manage():
    """Membersip Manage route."""
    if current_user.is_member():
        membership = current_user.get_membership()
        renewal = current_user.get_renewal()

        membership_prices = get_prices()

        price = None
        if renewal and renewal.renew_type == "m":
            price = membership_prices["monthly"]["m"]
        if renewal and renewal.renew_type == "y":
            price = membership_prices["annually"]["y"]

        form = ManageForm()
        if form.validate_on_submit():
            if form.cancel.data:
                current_user.remove_renewals()

                # email the user
                html_body = MIMEText(render_template(
                    "cancel.html", user=current_user
                ), "html")
                send_email("Membership Cancellation", current_user.email, html_body)

                flash("Your membership renewal has been successfully cancelled. We're sorry to see you go and hope you will consider rejoining soon.", "success")
                return redirect(url_for("membership_bp.membership_manage"))

            billing_option = None
            price = 0
            description = ""
            if form.renew_monthly.data and not form.renew_annually.data:
                billing_option = "m"
                #price = prices["monthly"]["m"]
                description = "Monthly Membership Renewal"
            elif not form.renew_monthly.data and form.renew_annually.data:
                billing_option = "y"
                #price = prices["annually"]["y"]
                description = "Annual Membership Renewal"
            else:
                return abort(400)

            # create a pending payment
            pending_payment = create_pending_payment(current_user.get_id(), price, description, {
                    "user_id": current_user.get_id(),
                    "billing_option": billing_option
                }, "renewal", "renewal")

            # assign pending payment to user session
            session["pending_payment"] = pending_payment.id

            # redirect the user for payment
            return redirect(url_for("pay_bp.pay"))

        return render_template(
            "membership_manage.html",
            form=form,
            membership=membership,
            renewal=renewal,
            price=price,
            prices=membership_prices
        )
    else:
        return redirect("/membership/join")

@membership_bp.route("/join", methods=["GET", "POST"])
@login_required
def membership_join():
    """Membership join route."""
    if current_user.is_member():
        return redirect("/membership/manage")

    prices = get_prices()

    form = BillingOptionForm()
    if form.validate_on_submit():
        billing_option = None
        price = 0
        description = ""
        if form.monthly.data and not form.annually.data:
            billing_option = "m"
            price = prices["monthly"]["m"]
            description = "Monthly Membership"
        elif not form.monthly.data and form.annually.data:
            billing_option = "y"
            price = prices["annually"]["y"]
            description = "Annual Membership"
        else:
            return redirect(url_for("membership_bp.membership_join"))

        # create a pending payment
        pending_payment = create_pending_payment(current_user.get_id(), price, description, {
                "user_id": current_user.get_id(),
                "billing_option": billing_option
            }, "membership", "membership")

        # assign pending payment to user session
        session["pending_payment"] = pending_payment.id

        # redirect the user for payment
        return redirect(url_for("pay_bp.pay"))

    return render_template(
        "membership_join.html", title="Join", form=form, prices=prices
    )


@membership_bp.route("/thankyou", methods=["GET"])
@login_required
def membership_thankyou():
    """Membersip thank you route."""
    return render_template("membership_thankyou.html")


@membership_bp.route("/goodbye", methods=["GET"])
@login_required
def membership_goodbye():
    """Membersip cancel route."""
    return render_template("membership_goodbye.html")
