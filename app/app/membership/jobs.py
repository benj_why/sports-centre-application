from app import db, models
from datetime import date, timedelta, datetime
from config import MEMBERSHIP_PRICE
import calendar
from flask import current_app
from app.membership.helpers import get_prices

from app.email import send_email
from email.mime.text import MIMEText

try:
    from flask_weasyprint import HTML, render_pdf
except:
    pass
from flask import render_template


def renew_memberships():

    if current_app.config["TESTING"] == False:
        print("Started renewing memberships...")

    # get todays date
    today = date.today()

    # query for all of the memberships that need renewing
    renewals = models.MembershipRenewal.query.filter(
        models.MembershipRenewal.renew_on
        >= datetime.combine(today, datetime.min.time())
    ).all()
    if current_app.config["TESTING"] == False:
        print("%d memberships need renewing..." % len(renewals))

    counts = {
        "success": 0,
        "failure": 0,
    }

    # for each membership renewal
    for renewal in renewals:
        prices = get_prices()

        # calculate end date and price
        end = None
        price = 0
        if renewal.renew_type == "m":
            days_in_month = calendar.monthrange(today.year, today.month)[1]
            end = today + timedelta(days=days_in_month)
            price = prices["monthly"]["m"]
        elif renewal.renew_type == "y":
            try:
                end = today.replace(year=today.year + 1)
            except ValueError:
                end = today + (date(today.year + 1, 1, 1) - date(today.year, 1, 1))
            price = prices["annually"]["y"]

        # get the card
        card = renewal.card
        if not card.take_card_payment(price):
            counts["failure"] += 1

            # email the user
            html_body = MIMEText(
                render_template("renewal_payment_error.html", user=renewal.user), "html"
            )
            try:
                send_email(
                    "Membership Renewal: Action Needed", renewal.user.email, html_body
                )
            except Exception as e:
                print("Error sending email")
                print(e.args[0])
        else:
            # add a membership renewal to the db
            new_renewal = models.MembershipRenewal(
                user_id=renewal.user.get_id(),
                card_id=card.id,
                renew_on=end + timedelta(days=1),
                renew_type=renewal.renew_type,
            )
            db.session.add(new_renewal)

            # add membership to db
            member = models.Membership(
                start_date=today, end_date=end, user_id=renewal.user.get_id()
            )
            db.session.add(member)

            db.session.commit()

            # email the user
            html_body = MIMEText(
                render_template(
                    "renewal_success.html", user=renewal.user, renewal=new_renewal
                ),
                "html",
            )
            try:
                send_email("Membership Renewal", renewal.user.email, html_body)
            except Exception as e:
                print(
                    "Couldn't send email due to to many in a short space of time (pretending it sent)"
                )
                print(e.args[0])
            counts["success"] += 1

        # remove the renewal order from the database

    # delete all of the expired membership orders
    db.session.query(models.MembershipRenewal).filter(
        models.MembershipRenewal.renew_on
        >= datetime.combine(today, datetime.min.time())
    ).delete()
    db.session.commit()

    if current_app.config["TESTING"] == False:
        print(
            "Finished renewing memberships. [%d successes and %d failures]"
            % (counts["success"], counts["failure"])
        )
