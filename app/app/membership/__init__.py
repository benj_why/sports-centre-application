from flask import current_app as app
from .jobs import renew_memberships
from apscheduler.schedulers.background import BackgroundScheduler
import atexit

try:
    @app.before_first_request
    def schedule_memberships():
        # schedule membership renewals
        scheduler = BackgroundScheduler()
        scheduler.add_job(func=renew_memberships, trigger="cron", hour="0")
        scheduler.start()
        atexit.register(lambda: scheduler.shutdown())


    @app.before_first_request
    def renew_memberships_start():
        # renew any memberships that need to be renewed
        renew_memberships()
        pass
except:
    pass
