import sys, os

import unittest

from ... import create_app, db
from ...user.views import create_user

from config import basedir
from flask import url_for, request
from flask import json
from flask_login import current_user, login_user, logout_user

from app import models

from datetime import datetime, date, timedelta
import calendar
 
class TestPay(unittest.TestCase):
    # set up before all tests
    @classmethod
    def setUpClass(cls):
        cls.do()

    @classmethod
    def do(self):
        self.app = create_app()                                                 
        self.app_context = self.app.test_request_context()
        self.app_context.push()
        self.client = self.app.test_client()
        self.app.config["TESTING"] = True
        self.app.config["WTF_CSRF_ENABLED"] = False
        self.app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(
            basedir, "test.db"
        )
        self.app.config["SEND_EMAILS"] = False
        db.app = self.app
        db.create_all()

    # set up prior to each test
    def setUp(self):
        # register user accounts
        db.init_app(self.app)
        with self.app.app_context():
            db.create_all()
            self.register("User", "One", "user1@email.com", "aaaaaa", "01234567890", "2000-04-27")
    
    # tear down after all tests
    @classmethod
    def tearDownClass(cls):
        db.session.remove()
        db.drop_all()

    # tear down after each test
    def tearDown(self):
        # clear all database data

        # meta = db.metadata
        # for table in reversed(meta.sorted_tables):
        #     db.session.execute(table.delete())
        # db.session.commit()

        db.init_app(self.app)
        with self.app.app_context():
            db.drop_all()
        db.session.commit()

    # helpers
    def login_test_nonmember(func):
        def wrapper(self):
            with self.client:
                self.login("user1@email.com", "aaaaaa")
                func(self)
                self.logout()
        return wrapper

    def login_test_member(func):
        def wrapper(self):
            with self.client:
                today = date.today()
                days_in_month = calendar.monthrange(today.year, today.month)[1]
                end = today + timedelta(days=days_in_month)
                m = models.Membership(
                    start_date=today, end_date=end, user_id=1
                )
                db.session.add(m)
                db.session.commit()

                self.login("user1@email.com", "aaaaaa")
                func(self)
                self.logout()
        return wrapper

    def register(self, first_name, last_name, email, password, number, dob):
        return self.client.post(
            "/user/signup",
            data=dict(
                first_name=first_name,
                last_name=last_name,
                email=email,
                password=password,
                emergency_number=number,
                dob=dob,
            ),
            follow_redirects=True
        )

    def login(self, email, password):
        return self.client.post("/user/login", data=dict(email=email, password=password))

    def logout(self):
        return self.client.get("/user/logout")

    def get_url(self, path):
        return "http://localhost" + url_for(path)

    def delete_all(self, model):
        model.query.delete()
        db.session.commit()

    def send_payment(self):
        # return an attempted payment using the provided details
        return self.client.post(
            url_for("pay_bp.pay"),
            data=dict(
                name_on_card="Test Card",
                card_number="4111111111111111",
                expiry_date="02/22",
                security_code="123",
                save_card="y",
                submit="Submit"
            ),
            follow_redirects=False
        )

    # tests

    # test that members are redirected when they visit the join page
    @login_test_member
    def test_member_cannot_join(self):
        # attempt to access join page
        response = self.client.get(url_for("membership_bp.membership_join"), follow_redirects=False)

        # check we are redirected
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, self.get_url("membership_bp.membership_manage"))

    # test that non members can access the join page
    @login_test_nonmember
    def test_membership_join_page(self):
        # attempt to access join page
        response = self.client.get(url_for("membership_bp.membership_join"), follow_redirects=False)

        # check we are not redirected
        self.assertEqual(response.status_code, 200)

    # test that nonmembers are redirected when they vist the manage page
    @login_test_nonmember
    def test_nonmember_cannot_manage(self):
        # attempt to access manage page
        response = self.client.get(url_for("membership_bp.membership_manage"), follow_redirects=False)

        # check we are redirected
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, self.get_url("membership_bp.membership_join"))

    # test that members can access the manage page
    @login_test_member
    def test_membership_manage_page(self):
        # attempt to access manage page
        response = self.client.get(url_for("membership_bp.membership_manage"), follow_redirects=False)

        # check we are not redirected
        self.assertEqual(response.status_code, 200)

    # test that users can create a monthly membership
    @login_test_nonmember
    def test_become_member_month(self):
        today = date.today()
        days_in_month = calendar.monthrange(today.year, today.month)[1]
        end = today + timedelta(days=days_in_month)

        # send request to become a member
        membership_response = self.client.post(
            url_for("membership_bp.membership_join"),
            data=dict(
                monthly="m",
                submit="submit"
            ),
            follow_redirects=False
        )

        pay_response = self.send_payment()

        memberships = models.Membership.query.all()
        self.assertTrue(len(memberships) == 1) # test only one membership created
        membership = memberships[0]

        self.assertTrue(membership.user_id == current_user.id)
        self.assertTrue(membership.start_date.date() == today) # check start date correct
        self.assertTrue(membership.end_date.date() == end) # check end date correct

        # test to make sure renewal is created

        renewals = models.MembershipRenewal.query.all()

        self.assertTrue(len(renewals) == 1) # test only one renewal created
        renewal = renewals[0]
        self.assertTrue(renewal.user_id == current_user.id)
        self.assertTrue(renewal.card_id == 1)
        self.assertTrue(renewal.renew_on.date() == end + timedelta(days=1)) # check renewal date is correct
        self.assertTrue(renewal.renew_type == "m") # check renewal type is correct

    # test that users can create an annual membership
    @login_test_nonmember
    def test_become_member_year(self):
        today = date.today()
        end = None
        try:
            end = today.replace(year=today.year + 1)
        except ValueError:
            end = today + (date(today.year + 1, 1, 1) - date(today.year, 1, 1))

        # send request to become a member
        membership_response = self.client.post(
            url_for("membership_bp.membership_join"),
            data=dict(
                annually="y",
                submit="submit"
            ),
            follow_redirects=False
        )

        pay_response = self.send_payment()

        memberships = models.Membership.query.all()
        self.assertTrue(len(memberships) == 1) # test only one membership created
        membership = memberships[0]

        self.assertTrue(membership.user_id == current_user.id)
        self.assertTrue(membership.start_date.date() == today) # check start date correct
        self.assertTrue(membership.end_date.date() == end) # check end date correct

        # test to make sure renewal is created

        renewals = models.MembershipRenewal.query.all()

        self.assertTrue(len(renewals) == 1) # test only one renewal created
        renewal = renewals[0]
        self.assertTrue(renewal.user_id == current_user.id)
        self.assertTrue(renewal.card_id == 1)
        self.assertTrue(renewal.renew_on.date() == end + timedelta(days=1)) # check renewal date is correct
        self.assertTrue(renewal.renew_type == "y") # check renewal type is correct


    # test that members can cancel their membership
    @login_test_nonmember
    def test_cancel_membership(self):
        # send request to become a member
        membership_response = self.client.post(
            url_for("membership_bp.membership_join"),
            data=dict(
                annually="y",
                submit="submit"
            ),
            follow_redirects=False
        )
        pay_response = self.send_payment()

        # send request to cancel renewal
        cancel_response = self.client.post(
            url_for("membership_bp.membership_manage"),
            data=dict(
                cancel="submit"
            ),
            follow_redirects=False
        )

        renewals = models.MembershipRenewal.query.all()

        self.assertTrue(len(renewals) == 0)

    # test that members without membership renewals can renew
    @login_test_member
    def test_renew_monthly(self):
        # send request to create renewal
        renew_response = self.client.post(
            url_for("membership_bp.membership_manage"),
            data=dict(
                renew_monthly="submit"
            ),
            follow_redirects=False
        )
        pay_response = self.send_payment()

        today = date.today()
        days_in_month = calendar.monthrange(today.year, today.month)[1]
        end = today + timedelta(days=days_in_month)

        renewals = models.MembershipRenewal.query.all()

        self.assertTrue(len(renewals) == 1) # test only one renewal created
        renewal = renewals[0]
        self.assertTrue(renewal.user_id == current_user.id)
        self.assertTrue(renewal.card_id == 1)
        self.assertTrue(renewal.renew_on.date() == end + timedelta(days=1)) # check renewal date is correct
        self.assertTrue(renewal.renew_type == "m") # check renewal type is correct

    # test that members without membership renewals can renew
    @login_test_member
    def test_renew_monthly(self):
        # send request to create renewal
        renew_response = self.client.post(
            url_for("membership_bp.membership_manage"),
            data=dict(
                renew_monthly="submit"
            ),
            follow_redirects=False
        )
        pay_response = self.send_payment()

        today = date.today()
        days_in_month = calendar.monthrange(today.year, today.month)[1]
        end = today + timedelta(days=days_in_month)

        renewals = models.MembershipRenewal.query.all()

        self.assertTrue(len(renewals) == 1) # test only one renewal created
        renewal = renewals[0]
        self.assertTrue(renewal.user_id == current_user.id)
        self.assertTrue(renewal.card_id == 1)
        self.assertTrue(renewal.renew_on.date() == end + timedelta(days=1)) # check renewal date is correct
        self.assertTrue(renewal.renew_type == "m") # check renewal type is correct

    # test that members without membership renewals can renew
    @login_test_member
    def test_renew_annually(self):
        # send request to create renewal
        renew_response = self.client.post(
            url_for("membership_bp.membership_manage"),
            data=dict(
                renew_annually="submit"
            ),
            follow_redirects=False
        )
        pay_response = self.send_payment()

        today = date.today()
        end = None
        try:
            end = today.replace(year=today.year + 1)
        except ValueError:
            end = today + (date(today.year + 1, 1, 1) - date(today.year, 1, 1))

        renewals = models.MembershipRenewal.query.all()

        self.assertTrue(len(renewals) == 1) # test only one renewal created
        renewal = renewals[0]
        self.assertTrue(renewal.user_id == current_user.id)
        self.assertTrue(renewal.card_id == 1)
        self.assertTrue(renewal.renew_on.date() == end + timedelta(days=1)) # check renewal date is correct
        self.assertTrue(renewal.renew_type == "y") # check renewal type is correct


if __name__ == "__main__":
    unittest.main()