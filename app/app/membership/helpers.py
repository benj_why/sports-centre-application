from config import MEMBERSHIP_PRICE

def get_prices():
    return {
        "monthly": {
            "m": MEMBERSHIP_PRICE["monthly"],
            "y": MEMBERSHIP_PRICE["monthly"] * 12,
        },
        "annually": {
            "m": MEMBERSHIP_PRICE["annual"] / 12,
            "y": MEMBERSHIP_PRICE["annual"],
        }
    }