from datetime import date, timedelta
import calendar
from app.models import Membership, MembershipRenewal
from app import db
from flask import flash

from app.email import send_email
from email.mime.text import MIMEText
try:
    from flask_weasyprint import HTML, render_pdf
except:
    pass
from flask import render_template


def membership_join(data, payment_card):
    # get todays date
    today = date.today()

    # calculate end date and price
    end = None
    if data["billing_option"] == "m":
        days_in_month = calendar.monthrange(today.year, today.month)[1]
        end = today + timedelta(days=days_in_month)
    elif data["billing_option"] == "y":
        try:
            end = today.replace(year=today.year + 1)
        except ValueError:
            end = today + (date(today.year + 1, 1, 1) - date(today.year, 1, 1))

    # add a membership renewal to the db
    renewal = MembershipRenewal(
        user_id=data["user_id"],
        card_id=payment_card.id,
        renew_on=end + timedelta(days=1),
        renew_type=data["billing_option"],
    )
    db.session.add(renewal)
    db.session.commit()

    # add membership to db
    member = Membership(start_date=today, end_date=end, user_id=data["user_id"])
    db.session.add(member)
    db.session.commit()

    # email the user
    html_body = MIMEText(render_template(
        "join_success.html", user=renewal.user, renewal=renewal
    ), "html")
    send_email("Membership", renewal.user.email, html_body)

    flash(
        "Thank you for your payment. You have successfully become a member!", "success"
    )

def renewal(data, payment_card):
    # get todays date
    today = date.today()

    # calculate end date and price
    end = None
    if data["billing_option"] == "m":
        days_in_month = calendar.monthrange(today.year, today.month)[1]
        end = today + timedelta(days=days_in_month)
    elif data["billing_option"] == "y":
        try:
            end = today.replace(year=today.year + 1)
        except ValueError:
            end = today + (date(today.year + 1, 1, 1) - date(today.year, 1, 1))

    # add a membership renewal to the db
    renewal = MembershipRenewal(
        user_id=data["user_id"],
        card_id=payment_card.id,
        renew_on=end + timedelta(days=1),
        renew_type=data["billing_option"],
    )
    db.session.add(renewal)
    db.session.commit()

    # email the user
    html_body = MIMEText(render_template(
        "renewal_success.html", user=renewal.user, renewal=renewal
    ), "html")
    send_email("Membership Renewal", renewal.user.email, html_body)

    flash(
        "Thank you! Your membership renewal was successfully created.", "success"
    )
