from flask import Flask, request, redirect, make_response, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_bcrypt import Bcrypt
from flask_wtf.csrf import CSRFProtect
from flask_login import LoginManager
from flask_qrcode import QRcode
from flask_bootstrap import Bootstrap
from functools import wraps

db = SQLAlchemy()
bcrypt = Bcrypt()
csrf = CSRFProtect()
migrate = Migrate()
login_manager = LoginManager()
qr = QRcode()


def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)

    # Application Configuration
    app.config.from_object("config")

    csrf.init_app(app)
    login_manager.init_app(app)

    bootstrap = Bootstrap(app)

    qr.init_app(app)
    bcrypt.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)

    with app.app_context():
        from app.models import User

        # keeps track of logged in user by storing indentities in flasks user session
        @login_manager.user_loader
        def load_user(id):
            return User.query.get(int(id))

        @login_manager.unauthorized_handler
        def handle_needs_login():
            if not request.referrer:
                resp = make_response(redirect(url_for("main_bp.index")))
            else:
                resp = make_response(redirect(request.referrer))

            resp.set_cookie("auth", "required")
            return resp

        # Import blueprints of our application
        from .admin.views import admin_bp
        from .main.views import main_bp
        from .user.views import user_bp
        from .membership.views import membership_bp
        from .error import errors_bp
        from .pay.views import pay_bp

        app.register_blueprint(admin_bp)
        app.register_blueprint(main_bp)
        app.register_blueprint(user_bp)
        app.register_blueprint(membership_bp)
        app.register_blueprint(errors_bp)
        app.register_blueprint(pay_bp)

        # login_manager.login_view = request.refferrer

    def confirmation_required(desc_fn):
        def inner(f):
            @wraps(f)
            def wrapper(*args, **kwargs):
                if request.args.get("confirm") != "1":
                    desc = desc_fn()
                    return redirect(
                        url_for("confirm", desc=desc, action_url=quote(request.url))
                    )
                return f(*args, **kwargs)

            return wrapper

        return inner

    return app
