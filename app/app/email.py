from flask import current_app as app
import smtplib
from email.mime.multipart import MIMEMultipart

# send an email given a subject, address, body and who its from
def send_email(subject, to_address, html_body, from_address="sport@theEdgier.com"):
    if not "SEND_EMAILS" in app.config or app.config["SEND_EMAILS"] == True:
        message = MIMEMultipart("alternative")
        message["Subject"] = subject
        message["From"] = from_address
        message["To"] = to_address
        message.attach(html_body)
        with smtplib.SMTP(
            app.config["MAIL_SMTP_SERVER"], app.config["MAIL_PORT"]
        ) as server:
            server.login(app.config["MAIL_LOGIN"], app.config["MAIL_PASSWORD"])
            server.sendmail(from_address, to_address, message.as_string())