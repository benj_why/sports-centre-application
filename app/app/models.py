from sqlalchemy.orm import backref
from werkzeug.security import generate_password_hash, check_password_hash
from flask import current_app, render_template
from flask_login import UserMixin
from app import db, bcrypt
from datetime import date, datetime, timedelta
from .email import send_email
from email.mime.text import MIMEText
import re

try:
    from flask_weasyprint import HTML, render_pdf
except:
    pass


class User(db.Model, UserMixin):
    id = db.Column(db.Integer(), primary_key=True)
    first_name = db.Column(db.String(128))
    last_name = db.Column(db.String(128))
    password = db.Column(db.String(128))
    emergency_number = db.Column(db.String(11))
    email = db.Column(db.String(128), unique=True)
    user_type = db.Column(db.Integer())  # 1=manager, 2=employee, 3=customer
    cards = db.relationship("Card", backref="user", lazy="dynamic")
    pending_payments = db.relationship("PendingPayment", backref="user", lazy="dynamic")
    memberships = db.relationship("Membership", backref="user", lazy="dynamic")
    membership_renewals = db.relationship(
        "MembershipRenewal", backref="user", lazy="dynamic"
    )
    bookings = db.relationship("Booking", backref="user", lazy="dynamic")
    payments = db.relationship("PaymentLog", backref="user", lazy="dynamic")

    dob = db.Column(db.DateTime())

    # activities = db.relationship("Booking")

    def __repr__(self):
        return "<User {}>".format(self.email)

    def set_password(self, password):
        self.password = bcrypt.generate_password_hash(password)

    def check_password(self, password_in):
        return bcrypt.check_password_hash(self.password, password_in)

    def get_id(self):
        return self.id

    def get_membership(self):
        m = (
            Membership.query.filter_by(user_id=self.get_id())
            .filter(Membership.end_date > date.today())
            .first()
        )
        return m

    def is_member(self):
        membership = self.get_membership()
        if membership == None:
            return False
        else:
            return True

    def is_employee(self):
        return self.user_type != 3

    def is_manager(self):
        return self.user_type == 1

    def get_renewal(self):
        r = MembershipRenewal.query.filter_by(user_id=self.get_id()).first()
        return r

    def remove_renewals(self):
        MembershipRenewal.query.filter(MembershipRenewal.user_id == self.id).delete()
        db.session.commit()

    def get_saved_cards(self):
        # return Card.query.filter_by(user_id=self.get_id()).all()
        return self.cards.filter(Card.user_saved == True).all()

    def get_pending_payments(self):
        return self.pending_payments.all()

    def get_pending_payment(self):
        return self.pending_payments.first()

    def has_pending_payment(self):
        return len(self.get_pending_payments()) > 0

    def reset_data(self):
        # clears out membership, renewal, and card data
        Membership.query.filter(Membership.user_id == self.id).delete()

        MembershipRenewal.query.filter(MembershipRenewal.user_id == self.id).delete()

        Card.query.filter(Card.user_id == self.id).delete()


class Membership(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    start_date = db.Column(db.DateTime())
    end_date = db.Column(db.DateTime())


class MembershipRenewal(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    card_id = db.Column(db.Integer, db.ForeignKey("card.id"))
    renew_on = db.Column(db.DateTime())
    renew_type = db.Column(db.String())


facility_activity_type = db.Table(
    "facility_activity_type",
    db.Column("facility_id", db.Integer, db.ForeignKey("facility.id")),
    db.Column("type_id", db.Integer, db.ForeignKey("activity_type.id")),
)


class Facility(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(128), unique=True)
    capacity = db.Column(db.Integer())
    activities = db.relationship("Activity", backref="facility", lazy=True)
    activity_types = db.relationship(
        "ActivityType", secondary=facility_activity_type, backref="facilities"
    )


class Booking(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey("user.id"))
    activity_id = db.Column(db.Integer(), db.ForeignKey("activity.id"))
    time_of_booking = db.Column(db.DateTime())

    def cancel(self):
        Booking.query.filter_by(id=self.id).delete()
        db.session.commit()


class Activity(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(128))
    start_time = db.Column(db.DateTime(), index=True)
    end_time = db.Column(db.DateTime())
    price = db.Column(db.Float())
    facility_id = db.Column(db.Integer(), db.ForeignKey("facility.id"), nullable=False)
    bookings = db.relationship("Booking", backref="activity", lazy="dynamic")

    def __repr__(self):
        return "<name {}, start: {}>".format(self.name, self.start_time)

    def is_peak_time(self):
        return (
            self.start_time.strftime("%A") == "Saturday"
            or self.start_time.strftime("%A") == "Sunday"
            or (
                self.start_time.strftime("%-H") >= "15"
                and self.start_time.strftime("%-H") < "17"
            )
        )

    def num_bookings(self):
        return len(list(self.bookings))

    def remaining_capacity(self):
        return self.facility.capacity - self.num_bookings()

    def initial_capacity(self):
        return self.facility.capacity

    def capacity_ratio(self):
        return self.remaining_capacity() / self.initial_capacity()


class ActivityType(db.Model):
    __tablename__ = "activity_type"
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(128))


class Card(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    card_number = db.Column(db.String())
    expiry_date = db.Column(db.DateTime())
    address = db.Column(db.String())
    postcode = db.Column(db.String())
    name_on_card = db.Column(db.String())
    security_code = db.Column(db.String())
    membership_renewals = db.relationship(
        "MembershipRenewal", backref="card", lazy="dynamic"
    )
    payments = db.relationship("PaymentLog", backref="card", lazy="dynamic")
    user_saved = db.Column(db.Boolean())

    def get_id(self):
        return id

    def take_card_payment(self, amount):
        # dummy function to take charge an amount to a card
        if current_app.config["TESTING"] == False:
            print(
                "** Simulated card payment of £%.2f from %s"
                % (amount, self.card_number)
            )
        return 1

    def get_anon_number(self):
        num_str = str(self.card_number)
        return_str = ""
        for i, digit in enumerate(num_str):
            if i % 4 == 0 and i != 0:
                return_str += " "

            if i < len(num_str) - 4:
                return_str += "*"
            else:
                return_str += digit
        return return_str

    def get_pretty_expiry_date(self):
        return self.expiry_date.strftime("%m/%y")


class PendingPayment(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    creation_time = db.Column(db.DateTime())
    price = db.Column(db.Integer())
    description = db.Column(db.String())
    data = db.Column(db.String())
    callback = db.Column(db.String())
    redirect = db.Column(db.String())

    def get_price_decimal(self):
        return self.price / 100

    def is_old(self):
        return self.creation_time < datetime.now() - timedelta(minutes=60)


class PaymentLog(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    card_id = db.Column(db.Integer, db.ForeignKey("card.id"))
    time = db.Column(db.DateTime())
    description = db.Column(db.String())
    price = db.Column(db.Integer())

    def get_price_decimal(self):
        return self.price / 100

    def send_email_receipt(self):
        subject = "Payment Receipt #%d" % self.id
        to_address = self.user.email
        html_body = MIMEText(self.get_payment_receipt("email"), "html")
        send_email(subject, to_address, html_body)

    def get_payment_receipt(self, content_type):
        html = render_template("payment_receipt.html", payment=self)
        if content_type == "html":
            return html
        if content_type == "pdf":
            return render_pdf(HTML(string=html))
        if content_type == "email":
            return re.sub("<nav.*?>.*?</nav>", "", html, flags=re.DOTALL)


class IPLog(db.Model):
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), primary_key=True)
    ip_addr = db.Column(db.String(32), primary_key=True)
