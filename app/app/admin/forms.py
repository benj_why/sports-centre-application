from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, SelectField
from wtforms.validators import DataRequired, Length, Email, ValidationError, EqualTo
from wtforms.fields.html5 import DateField, EmailField

from app.models import User, ActivityType, Facility


class SelectActivityType(FlaskForm):
   
    activity = SelectField('activity', coerce=str, validators=[DataRequired()])


    def __init__(self, facility):
        super().__init__()
        acitivity_types = (
                ActivityType.query
                    .join(ActivityType.facilities) 
                    .filter(Facility.name == facility)
            )
        self.activity.choices = [(i.name, i.name) for i in acitivity_types]


class SubmitDataView(FlaskForm):

    activity = SelectField('activity', coerce=str, validators=[DataRequired()])

    start_date = DateField("Start date", validators=[DataRequired()])
    end_date = DateField("End data", validators=[DataRequired()])

    def __init__(self, facility):
        super().__init__()
        activities = [ActivityType(name="all")]
        if facility :
            activities.extend((
                ActivityType.query
                    .join(ActivityType.facilities) 
                    .filter(Facility.name == facility)
            ))
        else:
            activities.extend((
                ActivityType.query.all()
            ))
        for act in activities:
            print (act)
        self.activity.choices = [(i.name, i.name) for i in activities]