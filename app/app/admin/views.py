from flask import Blueprint, render_template, jsonify, redirect, request
from flask import current_app as app
from app.models import User, Activity, Facility, ActivityType
from datetime import date, timedelta, datetime
from app.admin.forms import SelectActivityType, SubmitDataView
from sqlalchemy.orm import joinedload, contains_eager, subqueryload
from app import db

import time

# Blueprint Configuration
admin_bp = Blueprint(
    "admin_bp",
    __name__,
    template_folder="templates",
    static_folder="static",
    url_prefix="/admin",
)


@admin_bp.route("/", methods=['GET', 'POST'])
def index():
    return render_template("adminHome.html")




@admin_bp.route("/view_timetable", methods=['GET', 'POST'])
def load_timetable_page():
    return render_template("manageTimetable.html")          


@admin_bp.route("/facility/<string:facility>", methods=['GET', 'POST'], defaults={'week_inc':0})
@admin_bp.route("/facility/<string:facility>/<int:week_inc>", methods=['GET', 'POST'])
def get_timetable(facility, week_inc):
    print(facility)
    
    delta = timedelta(days=1)
    w_delta = timedelta(weeks=1)
    dayDate = date.today() + (week_inc*w_delta)
    # get the upcoing week in day from the db
    activities = Activity.query.filter(Activity.start_time >= dayDate, Activity.start_time <= 
            (dayDate + 6 * delta)).all()

    def gen_empty_activty(today, delta):
        return Activity(start_time=(today+delta))

    #generate timetable
    timetable = [[gen_empty_activty(dayDate, (y*delta)) for i in range(9)] for y in range(7)]
    
    
    for activity in activities:
        if activity.facility.name == facility:
            timetable[(activity.start_time.date() - dayDate).days][activity.start_time.hour - 9] = activity


    return jsonify(
                    {
                        "status": "OK",
                        "data":render_template("adminTimetable.html",
                                                timetable=timetable,
                                                facility=facility,
                                                week_inc=week_inc)
                    }
                )




@admin_bp.route("/facility/<string:facility>/<int:week>/<int:day>/<int:hour>", methods=['GET', 'POST'])
def assign_slot(facility, week, day, hour):
    print(facility, day, hour)
    activity_form = SelectActivityType(facility)

    if request.method == "POST":
        
        print(activity_form.activity.data)
        if activity_form.validate_on_submit():
            facility_obj = Facility.query.filter_by(name=facility).first()

            day_date = date.today() + timedelta(days=day-1) + (week*timedelta(weeks=1))
            start_time = datetime(
                year=day_date.year, 
                month=day_date.month,
                day=day_date.day,
                hour=8+hour
            )

            activity = Activity(name=activity_form.activity.data,
                                start_time=start_time,
                                end_time=start_time+timedelta(hours=1),
                                facility_id=facility_obj.id)

            print(activity)

            db.session.add(activity)
            db.session.commit()

            return get_timetable(facility, week)


        else:
            return jsonify(
                {
                    "status": "BAD",
                    "data":render_template("bookActivity.html", 
                                            activity_form=activity_form,
                                            facility=facility,
                                            week=week,
                                            day=day,
                                            hour=hour)
                }
            )
 
    return jsonify(
                    {
                        "status": "OK",
                        "data":render_template("bookActivity.html",
                                                activity_form=activity_form,
                                                facility=facility,
                                                week=week,
                                                day=day,
                                                hour=hour)
                    }
                )




@admin_bp.route("/facility/<int:activity_id>", methods=['GET', 'POST'])
def manage_slot(activity_id):

    activity = Activity.query.get(activity_id)


    return jsonify(
                    {
                        "status": "OK",
                        "data":render_template("viewActivity.html",
                                                activity=activity)
                    }
                )






@admin_bp.route("/view_statistics", methods=['GET', 'POST'])
def load_statistics_page():
    return render_template("statistics.html")



## --------------------------------- GET CHART DATA ------------------------------------ ##


@admin_bp.route("/stats",methods=['GET', 'POST'], defaults={'facility':None, 'activity':None})
@admin_bp.route("/stats/<string:facility>",methods=['GET', 'POST'], defaults={'activity':None})
@admin_bp.route("/stats/<string:facility>/<string:activity>",methods=['GET', 'POST'])
def get_stats(facility, activity):

    months =3
    dayDate = date.today()
    delta = timedelta(days=1)
    w_delta = timedelta(weeks=1)

    start = time.time()

    offset = 6 - dayDate.weekday()
    start_date = dayDate + (offset*delta)
    
    i = 0
    labels = []
    while i < months*4:
        labels.insert(0,start_date - (i*w_delta))
        i += 1 

    
    if facility == None:
        activities = (
            Activity.query
                .options(subqueryload(Activity.users))
                .filter(Activity.start_time > (labels[len(labels)-1] - timedelta(weeks=4*months)),
                        Activity.start_time <= labels[len(labels)-1])
                .all()
        )
    elif activity == None:
        activities = (
                    Activity.query
                        .join(Activity.users)
                        .options(contains_eager('users'))
                        .join(Activity.facility) 
                        .filter(Activity.start_time > (labels[len(labels)-1] - timedelta(weeks=4*months)),
                                Activity.start_time <= labels[len(labels)-1])
                        .filter(Facility.name == facility)
                        .all()
                )
    else:
        activities = (
                    Activity.query
                        .join(Activity.facility)
                        .filter(Activity.name == Activity)
                        .filter(Activity.start_time > (labels[len(labels)-1] - timedelta(weeks=4*months)),
                                Activity.start_time <= labels[len(labels)-1])
                        .filter(Facility.name == facility)
                        .all()
                )
    
    
    if facility == None:
        weekly_data, activities = generate_chart_data(labels, activities, False)
    else:
        weekly_data, activities = generate_chart_data(labels, activities, True)

    print ("START")

    view_form = SubmitDataView(facility)
    view_form.end_date.data = labels[len(labels)-1]
    view_form.start_date.data = labels[len(labels)-2] + delta
    next_url = 2 if len(activities)>20 else None

    print ("ENDING ")
    print(len(activities))
    return jsonify(
                    {
                        "status": "OK",
                        "labels":[x.strftime("%b %d %Y") for x in labels],
                        "data":weekly_data,
                        "activities": render_template("activityList.html",
                            view_form=view_form,
                            activities=activities[:20],
                            facility=facility,
                            next_url=next_url,
                            prev_url=1)
                    }
                )




def generate_chart_data(labels, activities, is_act_type):

    activities = sorted(
        activities,
        key=lambda x: x.start_time
    )

    week_count = 0
    label_index = 0

    user_counts = {"overall":[]}
    activity_list = []
    
    for i, activity in enumerate(activities):

        if activity.start_time.date() >= labels[label_index]:
            user_counts['overall'].append(week_count)
            week_count = 0
            label_index += 1

        week_count += len(activity.users)

        if is_act_type:
            if not activity.name in user_counts:
                user_counts[activity.name] = [0 for i in range(len(labels))]
            user_counts[activity.name][label_index] += len(activity.users)
        else:
            if not activity.facility_id in user_counts:
                user_counts[activity.facility_id] = [0 for i in range(len(labels))]
            user_counts[activity.facility_id][label_index] += len(activity.users)

        if label_index == len(labels)-2:
            activity_list.append(activity)

        if i == len(activities)-1:
            user_counts['overall'].append(week_count)
            break

    if not is_act_type:
        user_counts = get_facilty_name(user_counts)
    
    return user_counts, list(reversed(activity_list))




def get_facilty_name(dict):
    new_dict = {}

    for fac_id in dict:
        if isinstance(fac_id, int):
            facility = Facility.query.get(fac_id)
            new_dict[facility.name] = dict[fac_id]
        else:
            new_dict[fac_id] = dict[fac_id]

    return new_dict



# ------------------------------------------------ per activity data view -------------------------------

@admin_bp.route("/stats/activity_history/",
                methods=['POST'],
                defaults={'facility':None,'page':1})
@admin_bp.route("/stats/activity_history/<string:facility>",
                methods=['POST'],
                defaults={'page':1})
@admin_bp.route("/stats/activity_history/<string:facility>/<int:page>",
                methods=['POST'])
def activity_history(facility,page ):

    ACTIVITY_VIEW_LENGTH = 20
    view_form = SubmitDataView(facility)

    start = view_form.start_date.data
    end = view_form.end_date.data
    activity = view_form.activity.data

    if facility == None:
        activities = (
            Activity.query
                .filter(Activity.start_time >= start,
                        Activity.start_time <= end)
                .paginate(page, ACTIVITY_VIEW_LENGTH, False)
        )
    elif activity == None:
        activities = (
                    Activity.query
                        .join(Activity.facility) 
                        .filter(Activity.start_time >= start,
                                Activity.start_time <= end)
                        .filter(Facility.name == facility)
                        .paginate(page, ACTIVITY_VIEW_LENGTH, False)
                )
    else:
        activities = (
                    Activity.query
                        .join(Activity.facility)
                        .filter(Activity.name == activity)
                        .filter(Activity.start_time >= start,
                                Activity.start_time <= end)
                        .filter(Facility.name == facility)
                        .paginate(page, ACTIVITY_VIEW_LENGTH, False)
                )

    next_url = activities.next_num \
        if activities.has_next else None
    prev_url = activities.prev_num \
        if activities.has_prev else None


    return jsonify(
                    {
                        "status": "OK",
                        "data": render_template("activityList.html",
                            view_form=view_form,
                            activities=activities.items,
                            facility=facility,
                            next_url=next_url,
                            prev_url=prev_url)
                    }
                )
                
