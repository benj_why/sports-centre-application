$(document).ready(function () {

    var csrf_token = $("meta[name=csrf-token]").attr("content");
    // Configure ajaxSetupso that the CSRF token is added to the header of every request
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (
                !/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) &&
                !this.crossDomain
            ) {
                xhr.setRequestHeader("X-CSRFToken", csrf_token);
            }
        }
    });

    //------------------------------ manage change page ---------------------

    $(document).on("click", ".change_admin_view", function () {
        $.ajax({
            url: $(this).attr("dest"),
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.status == "OK") {
                    $("#admin_content")
                        .empty();
                    $("#admin_content")
                        .append(response.data);
                } else if (response.status == "BAD") {
                    console.log("bad error");
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    })


    // ------------------------------ timetable ------------------------------

    $(document).on("click", ".getAdminTimetable", function () {
        $.ajax({
            url: $(this).attr("dest"),
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.status == "OK") {
                    $("#timetableContainer")
                        .empty();
                    $("#timetableContainer")
                        .append(response.data);
                } else if (response.status == "BAD") {
                    console.log("bad error");
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    })



    $(document).on("click", ".available_activity", function () {
        $.ajax({
            url: $(this).attr("dest"),
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.status == "OK") {
                    $("#dynamicModalInsert").append(response.data);
                } else if (response.status == "BAD") {
                    console.log("bad error");
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    })




    $(document).on("click", ".view_activity", function () {
        console.log($(this).attr("dest"))
        $.ajax({
            url: $(this).attr("dest"),
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.status == "OK") {
                    console.log(response.data)
                    $("#dynamicModalInsert").append(response.data);
                } else if (response.status == "BAD") {
                    console.log("bad error");
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    })


    $(document).on("submit", ".book_activity_slot", function (e) {
        $.ajax({
            url: $(this).attr("action"),
            type: "POST",
            data: $(this).serialize(),
            success: function (response) {
                if (response.status == "OK") {

                    $("#dynamicModal").toggle();
                    $("#dynamicModal").click();
                    $("#timetableContainer")
                        .empty();
                    $("#timetableContainer")
                        .append(response.data);
                } else if (response.status == "BAD") {
                    $("#dynamicModalInsert")
                        .empty();
                    $("#dynamicModalInsert")
                        .append(response.data);
                }
            },
            error: function (error) {
                console.log("errore", error);
            }
        });
        e.preventDefault();
    })


    $(document).on("submit", "#view_data_form", function (e) {
        $('#loading_spinner').show();
        $.ajax({
            url: $(this).attr("action"),
            type: "POST",
            data: $(this).serialize(),
            success: function (response) {
                if (response.status == "OK") {
                    $('#loading_spinner').hide();
                    //$("#activity_listy_container").empty();
                    let children = $("#activity_listy_container").children();
                    $("#activity_listy_container")
                        .prepend(response.data).hide().fadeIn(1000);
                    children.fadeOut(1000).remove();
                } else if (response.status == "BAD") {
                    console.log("bad errro");
                }
            },
            error: function (error) {
                console.log("errore", error);
            }
        });
        e.preventDefault();
    })

})