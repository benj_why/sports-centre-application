$(document).ready(function () {

    var csrf_token = $("meta[name=csrf-token]").attr("content");
    // Configure ajaxSetupso that the CSRF token is added to the header of every request
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (
                !/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) &&
                !this.crossDomain
            ) {
                xhr.setRequestHeader("X-CSRFToken", csrf_token);
            }
        }
    });

    console.log("this should load")
    // ------------------- CHART HELPER -------------------------------------------

    function build_chart(response) {
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: response.labels,
                datasets: build_dataset(response.data)
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }

    function build_dataset(data) {
        output = []
        console.log(data)
        console.log(Object.keys(data));
        for (var key of Object.keys(data)) {
            console.log(key)
            output.push({
                data: data[key],
                label: key,
                borderColor: '#' + (Math.random() * 0xFFFFFF << 0).toString(16),
                fill: false
            })
        }
        console.log(output)
        return output
    }




    // -------------------- download the chart data on page load -------------------

    page_url = $("meta[name=stat_data_url]").attr("dest");

    $.ajax({
        url: page_url,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.status == "OK") {
                build_chart(response);
                $("#activity_listy_container").append(response.activities);
                $('#loading_spinner').hide();
            } else if (response.status == "BAD") {
                console.log("bad error");
            }
        },
        error: function (error) {
            console.log(error)
        }
    });



    $(document).on("click", ".download_chart_data", function () {
        var chart = $("#myChart");
        $('#loading_spinner').show();
        $.ajax({
            url: $(this).attr("dest"),
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.status == "OK") {
                    chart.replaceWith('<canvas id="myChart" width="600" height="400"></canvas>')
                    build_chart(response)
                    $('#loading_spinner').hide();
                    $("#activity_listy_container").empty()
                    $("#activity_listy_container").append(response.activities)
                } else if (response.status == "BAD") {
                    console.log("bad error");
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    })


})