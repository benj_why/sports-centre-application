from flask import url_for
from app.models import PendingPayment
from app import db
import json

membership_join_callback = None
try:
    from app.membership.callbacks import membership_join as membership_join_callback
except:
    pass

membership_renewal_callback = None
try:
    from app.membership.callbacks import renewal as membership_renewal_callback
except:
    pass

booking_callback = None
try:
    from app.main.callbacks import create_booking as booking_callback
except:
    pass

test_callback = None
try:
    from app.pay.tests.callbacks import test_callback as test_callback
except:
    pass

from datetime import datetime


def example_callback(data, payment_card):
    print("Example callback!")


callbacks = {
    "example": example_callback,
    "membership": membership_join_callback,
    "renewal": membership_renewal_callback,
    "booking": booking_callback,
    "test": test_callback
}

redirections = {
    "example": "pay_bp.thankyou",
    "membership": "membership_bp.membership_manage",
    "renewal": "membership_bp.membership_manage",
    "booking": "pay_bp.thankyou",
    "test": "pay_bp.thankyou",
}


def create_pending_payment(userid, price, description, data, callback, redirect):
    # remove any existing pending payments for the user
    pending_payments = PendingPayment.query.filter_by(user_id=userid).delete()
    db.session.commit()

    # create the new payment
    pp = PendingPayment(
        user_id=userid,
        creation_time=datetime.now(),
        price=price * 100,
        description=description,
        data=json.dumps(data),
        callback=callback,
        redirect=redirect,
    )
    db.session.add(pp)
    db.session.commit()

    return pp
