from flask import (
    Blueprint,
    render_template,
    jsonify,
    request,
    redirect,
    url_for,
    session,
    abort,
)
from flask import current_app as app
from flask_login import current_user, login_user, logout_user, login_required

from .forms import PaymentForm, ExamplePaymentForm
from app.models import User, Card, PendingPayment, PaymentLog
from app import db
from .payments import create_pending_payment
from .payments import callbacks as payment_callbacks
from .payments import redirections as payment_redirections
import json
from datetime import datetime
from app.decorators import debug_only

# Blueprint Configuration
pay_bp = Blueprint(
    "pay_bp",
    __name__,
    template_folder="templates",
    static_folder="static",
    url_prefix="/pay",
)


@pay_bp.route("/", methods=["GET", "POST"])
@login_required
def pay():
    # verify the user has got a pending payment
    # pending_payment_id = session.get("pending_payment", None)
    # if pending_payment_id == None:
    if not current_user.has_pending_payment():
        return render_template(
            "payment_error.html",
            title="Payment Error",
            message="You have no pending payment!",
        ), 400

    pending_payment = current_user.get_pending_payment()

    # find the pending payment
    # pending_payment = PendingPayment.query.filter_by(id=pending_payment_id).first()
    if pending_payment == None:
        return render_template(
            "payment_error.html",
            title="Payment Error",
            message="We couldn't find your pending payment in our database.",
        ), 400

    # a user can only pay for their own things
    if pending_payment.user_id != current_user.get_id():
        return render_template(
            "payment_error.html",
            title="Payment Error",
            message="You can't pay for another user's purchase.",
        ), 403

    cards = current_user.get_saved_cards()

    form = PaymentForm()
    if form.validate_on_submit():
        # take payment from user
        save_card = True if form.save_card.data else False
        security_code = form.security_code.data

        user_card = Card(
            user_id=current_user.get_id(),
            card_number=form.card_number.data,
            expiry_date=form.expiry_date.data,
            name_on_card=form.name_on_card.data,
            user_saved=save_card,
            security_code=security_code,
        )

        payment_success = user_card.take_card_payment(
            pending_payment.get_price_decimal()
        )
        if not payment_success:
            # return a failure message
            flash(
                "Payment unsuccessful, please try again or, if the issue continues, try a different card",
                "error",
            )
            return redirect(url_for("pay_bp.pay"))

        # save card details
        db.session.add(user_card)
        db.session.commit()

        # log the transaction
        log = PaymentLog(
            user_id=current_user.get_id(),
            card_id=user_card.id,
            time=datetime.now(),
            description=pending_payment.description,
            price=pending_payment.price,
        )
        db.session.add(log)
        db.session.commit()

        # send a receipt
        log.send_email_receipt()

        # payment is no longer pending so delete it as such
        session["pending_payment"] = None
        db.session.delete(pending_payment)
        db.session.commit()

        # execute the callback
        data = json.loads(pending_payment.data)
        payment_callbacks[pending_payment.callback](data, user_card)

        # send success to the user
        return redirect(url_for(payment_redirections[pending_payment.redirect]))

    return render_template(
        "payment-form.html",
        title="Pay",
        form=form,
        cards=cards,
        payment=pending_payment,
    )


@pay_bp.route("/example", methods=["GET", "POST"])
@debug_only
@login_required
def example_pay():
    item = {"desc": "An Example Product", "price": 2.99}

    form = ExamplePaymentForm()
    if form.validate_on_submit():
        # create a pending payment
        pending_payment = create_pending_payment(
            current_user.get_id(), item["price"], item["desc"], {}, "example", "example"
        )

        # assign pending payment to user session
        session["pending_payment"] = pending_payment.id

        # redirect the user for payment
        return redirect("/pay")

    return render_template(
        "example.html", title="Example Payment", form=form, item=item
    )


@pay_bp.route("/cards/<card_id>", methods=["DELETE"])
@login_required
def delete_card(card_id):
    card = Card.query.filter(Card.id == card_id).first()
    
    if not card:
        return abort(404) 
    if not card.user_id == current_user.id:
        return abort(403)
    
    #card.delete()
    db.session.delete(card)
    db.session.commit()

    return jsonify({"success": True})


@pay_bp.route("/thankyou", methods=["GET"])
@login_required
def thankyou():
    return render_template("thankyou.html", title="Thank You!")


@app.before_request
def check_pending_payments():
    if current_user.is_authenticated:

        pending_payments = current_user.get_pending_payments()

        # check if user has a pending payment
        if len(pending_payments) > 0:
            # check if the pending payment is old
            for payment in pending_payments:
                if payment.is_old():
                    print(True)
                    # delete the pending payment
                    db.session.delete(payment)
                    db.session.commit()
