from flask_wtf import FlaskForm
from wtforms import (
    RadioField,
    IntegerField,
    DateField,
    TextField,
    SubmitField,
    BooleanField,
)
from wtforms.validators import DataRequired, ValidationError, Regexp
from cardvalidator import luhn

# Length validator for user input strings
class Length(object):
    def __init__(self, min=-1, max=-1, message=None):
        self.min = min
        self.max = max
        if not message:
            message = u"Field must be between %i and %i characters long." % (min, max)
        self.message = message

    def __call__(self, form, field):
        l = field.data and len(field.data) or 0
        if l < self.min or self.max != -1 and l > self.max:
            raise ValidationError(self.message)


length = Length

def validate_card(form, field):
    if not luhn.is_valid(field.data):
        raise ValidationError("Card Number must be a valid card number")

def filter_card_number(value):
    if value:
        return value.replace(" ", "")
    else:
        return value

class PaymentForm(FlaskForm):
    card_number = TextField(
        "Card Number",
        validators=[
            DataRequired(),
            validate_card,
        ],
        filters=[filter_card_number]
    )
    expiry_date = DateField("Expiry Date", format="%m/%y", validators=[DataRequired()])
    name_on_card = TextField(
        "Name on Card",
        validators=[
            DataRequired(),
            length(1, 30, "Name must be 30 characters or fewer"),
        ],
    )
    security_code = TextField(
        "Security Code", validators=[DataRequired(), Regexp("^[0-9]{3,4}$")]
    )
    save_card = BooleanField("Save Card Details")
    submit = SubmitField("Submit", validators=[DataRequired()])


class ExamplePaymentForm(FlaskForm):
    submit = SubmitField("Submit", validators=[DataRequired()])
