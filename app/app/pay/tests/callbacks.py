from app import db
from app.models import Membership
from datetime import datetime

def test_callback(data, payment_card):
    # very hacky way of allowing the test file to detect a successful callback
    m = Membership(
        user_id = 1,
        start_date = datetime.now(),
        end_date = datetime.now()
    )
    db.session.add(m)
    db.session.commit()