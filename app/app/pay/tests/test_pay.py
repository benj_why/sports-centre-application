import sys, os

import unittest

from ... import create_app, db
from ...user.views import create_user

from config import basedir
from flask import url_for, request
from flask import json
from flask_login import current_user, login_user, logout_user

from app import models

from ..payments import create_pending_payment
from ..payments import redirections

import datetime
 
class TestMembership(unittest.TestCase):
    # set up before all tests
    @classmethod
    def setUpClass(cls):
        cls.do()

    @classmethod
    def do(self):
        self.app = create_app()                                                 
        self.app_context = self.app.test_request_context()
        self.app_context.push()
        self.client = self.app.test_client()
        self.app.config["TESTING"] = True
        self.app.config["WTF_CSRF_ENABLED"] = False
        self.app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(
            basedir, "test.db"
        )
        self.app.config["SEND_EMAILS"] = False
        db.app = self.app
        db.create_all()

    # set up prior to each test
    def setUp(self):
        # register user accounts
        db.init_app(self.app)
        with self.app.app_context():
            db.create_all()
            self.register("User", "One", "user1@email.com", "aaaaaa", "01234567890", "2000-04-27")
    
    # tear down after all tests
    @classmethod
    def tearDownClass(cls):
        db.session.remove()
        db.drop_all()

    # tear down after each test
    def tearDown(self):
        # clear all database data

        # meta = db.metadata
        # for table in reversed(meta.sorted_tables):
        #     db.session.execute(table.delete())
        # db.session.commit()

        db.init_app(self.app)
        with self.app.app_context():
            db.drop_all()
        db.session.commit()

    # helpers
    def login_test(func):
        def wrapper(self):
            with self.client:
                self.login("user1@email.com", "aaaaaa")
                func(self)
                self.logout()
        return wrapper

    def register(self, first_name, last_name, email, password, number, dob):
        return self.client.post(
            "/user/signup",
            data=dict(
                first_name=first_name,
                last_name=last_name,
                email=email,
                password=password,
                emergency_number=number,
                dob=dob,
            ),
            follow_redirects=True
        )

    def login(self, email, password):
        return self.client.post("/user/login", data=dict(email=email, password=password))

    def logout(self):
        return self.client.get("/user/logout")

    def get_url(self, path):
        return "http://localhost" + url_for(path)

    def create_test_payment(self, description, price, callback, redirect):
        # create a pending payment
        pending_payment = create_pending_payment(
           current_user.get_id(), price, description, {}, callback, redirect
        )

    def create_test_payment_custom_user(self, description, price, callback, redirect, user_id):
        # create a pending payment
        pending_payment = create_pending_payment(
           user_id, price, description, {}, callback, redirect
        )

    def custom_payment_test(self,
                name_on_card="Test Name",
                card_number="4111111111111111",
                expiry_date="07/22",
                security_code="123",
                save_card=None
            ):

        # set up a payment
        self.create_test_payment("Test Payment", 2.99, "test", "test")

        # return an attempted payment using the provided details
        return self.client.post(
            url_for("pay_bp.pay"),
            data=dict(
                name_on_card=name_on_card,
                card_number=card_number,
                expiry_date=expiry_date,
                security_code=security_code,
                save_card=save_card,
                submit="Submit"
            ),
            follow_redirects=False
        )

    def check_card_structure(self, card):
        if card.name_on_card != "Test Name":
            return False

        if card.card_number != "4111111111111111":
            return False
        
        if card.expiry_date != datetime.datetime(2022, 7, 1):
            return False

        if card.security_code != "123":
            return False

        if card.user_saved != True and card.user_saved != False:
            return False

        return True

    def delete_all(self, model):
        model.query.delete()
        db.session.commit()

    # tests

    # test error when no pending payments
    @login_test
    def test_no_pending_payment(self):
        # send the payments form
        response = self.client.get(
            url_for("pay_bp.pay"),
            follow_redirects=False
        )

        self.assertTrue(response.status_code == 400)

        # send the payments form
        response = self.client.post(
            url_for("pay_bp.pay"),
            data=dict(
                name_on_card="Test Name",
                card_number="4111111111111111",
                expiry_date="07/22",
                security_code="123",
                save_card="n",
                submit="Submit"
            ),
            follow_redirects=False
        )

        self.assertTrue(response.status_code == 400)

    # test page works when pending payments
    @login_test
    def test_pending_payment_get(self):
        # set up the payment
        self.create_test_payment("Test Payment", 2.99, "test", "test")

        # send the payments form
        response = self.client.get(
            url_for("pay_bp.pay"),
            follow_redirects=False
        )

        self.assertTrue(response.status_code == 200)
    

    # test successful payment
    @login_test
    def test_correct_payment(self):
        # set up the payment
        self.create_test_payment("Test Payment", 2.99, "test", "test")

        # send the payments form
        response = self.client.post(
            url_for("pay_bp.pay"),
            data=dict(
                name_on_card="Test Name",
                card_number="4111111111111111",
                expiry_date="07/22",
                security_code="123",
                save_card="n",
                submit="Submit"
            ),
            follow_redirects=False
        )

        # check that payment was completed
        logs = models.PaymentLog.query.all()
        self.assertTrue(len(logs) == 1)

        # check that pending payment is removed
        pending_payments = models.PendingPayment.query.all()
        self.assertTrue(len(pending_payments) == 0)

    # test that pending payment is created
    @login_test
    def test_create_pending_payment(self):
        # set up the payment
        desc = "Test Payment"
        price = 2.99
        callback = "test"
        redirect = "test"
        self.create_test_payment(desc, price, callback, redirect)

        # check that payment was completed
        pending_payments = models.PendingPayment.query.all()
        self.assertTrue(len(pending_payments) == 1)

        pending_payment = pending_payments[0]
        
        self.assertTrue(pending_payment.user.id == current_user.id)
        self.assertTrue(isinstance(pending_payment.creation_time, datetime.datetime))
        self.assertTrue(pending_payment.price == price * 100)
        self.assertTrue(pending_payment.description == desc)
        self.assertTrue(pending_payment.data == "{}")
        self.assertTrue(pending_payment.callback == callback)
        self.assertTrue(pending_payment.redirect == redirect)

    # test error checking on name
    @login_test
    def test_name_errors(self):
        # test correct cases
        response = self.custom_payment_test()
        self.assertTrue(len(models.PaymentLog.query.all()) == 1)
        self.delete_all(models.PaymentLog)
        
        response = self.custom_payment_test(name_on_card = "0")
        self.assertTrue(len(models.PaymentLog.query.all()) == 1)
        self.delete_all(models.PaymentLog)

        response = self.custom_payment_test(name_on_card = "012345678901234567890123456789")
        self.assertTrue(len(models.PaymentLog.query.all()) == 1)
        self.delete_all(models.PaymentLog)

        # test error cases
        response = self.custom_payment_test(name_on_card = None)
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()

        response = self.custom_payment_test(name_on_card = "")
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()

        response = self.custom_payment_test(name_on_card = "0123456789012345678901234567890")
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()

        response = self.custom_payment_test(name_on_card = "01234567890123456789012345678901234567890123456789")
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()


    # test error checking on card number
    @login_test
    def test_card_number_errors(self):
        # test correct cases
        response = self.custom_payment_test()
        self.assertTrue(len(models.PaymentLog.query.all()) == 1)
        self.delete_all(models.PaymentLog)
        
        response = self.custom_payment_test(card_number = "5555555555554444")
        self.assertTrue(len(models.PaymentLog.query.all()) == 1)
        self.delete_all(models.PaymentLog)

        response = self.custom_payment_test(card_number = "378282246310005")
        self.assertTrue(len(models.PaymentLog.query.all()) == 1)
        self.delete_all(models.PaymentLog)

        response = self.custom_payment_test(card_number = "4222222222222")
        self.assertTrue(len(models.PaymentLog.query.all()) == 1)
        self.delete_all(models.PaymentLog)

        response = self.custom_payment_test(card_number = "4111 1111 1111 1111")
        self.assertTrue(len(models.PaymentLog.query.all()) == 1)
        self.delete_all(models.PaymentLog)

        # test error cases
        response = self.custom_payment_test(card_number = None)
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()

        response = self.custom_payment_test(card_number = "")
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()

        response = self.custom_payment_test(card_number = "4111111111111112")
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()
    
    # test error checking on card expiry
    @login_test
    def test_card_expiry_errors(self):
        # test correct cases
        response = self.custom_payment_test()
        self.assertTrue(len(models.PaymentLog.query.all()) == 1)
        self.delete_all(models.PaymentLog)
        
        response = self.custom_payment_test(expiry_date = "2/25")
        self.assertTrue(len(models.PaymentLog.query.all()) == 1)
        self.delete_all(models.PaymentLog)

        # test error cases
        response = self.custom_payment_test(card_number = None)
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()

        response = self.custom_payment_test(card_number = "")
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()

        response = self.custom_payment_test(card_number = "02.22")
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()

        response = self.custom_payment_test(card_number = "00/22")
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()

        response = self.custom_payment_test(card_number = "13/22")
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()

        response = self.custom_payment_test(card_number = "02/2022")
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()

        response = self.custom_payment_test(card_number = "a")
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()

        response = self.custom_payment_test(card_number = "a/a")
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()

    # test error checking on security code
    @login_test
    def test_security_code_errors(self):
        # test correct cases
        response = self.custom_payment_test()
        self.assertTrue(len(models.PaymentLog.query.all()) == 1)
        self.delete_all(models.PaymentLog)
        
        response = self.custom_payment_test(security_code = "123")
        self.assertTrue(len(models.PaymentLog.query.all()) == 1)
        self.delete_all(models.PaymentLog)

        response = self.custom_payment_test(security_code = "1234")
        self.assertTrue(len(models.PaymentLog.query.all()) == 1)
        self.delete_all(models.PaymentLog)

        # test error cases
        response = self.custom_payment_test(security_code = None)
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()

        response = self.custom_payment_test(security_code = "")
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()

        response = self.custom_payment_test(security_code = "1")
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()

        response = self.custom_payment_test(security_code = "12")
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()

        response = self.custom_payment_test(security_code = "12345")
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()

        response = self.custom_payment_test(security_code = "aaa")
        self.assertTrue(len(models.PaymentLog.query.all()) == 0)
        #models.PaymentLog.query.delete()
    
    # test card saving
    @login_test
    def test_save_card_errors(self):
        # test correct cases
        response = self.custom_payment_test()
        self.assertTrue(len(models.PaymentLog.query.all()) == 1)
        card = models.Card.query.first()
        self.assertTrue(card != None)
        self.assertTrue(self.check_card_structure(card))
        self.assertTrue(card.user_saved == False)
        self.delete_all(models.Card)
        self.delete_all(models.PaymentLog)
        
        response = self.custom_payment_test(save_card = "y")
        self.assertTrue(len(models.PaymentLog.query.all()) == 1)
        card = models.Card.query.first()
        self.assertTrue(card != None)
        self.assertTrue(self.check_card_structure(card))
        self.assertTrue(card.user_saved == True)
        self.delete_all(models.Card)
        self.delete_all(models.PaymentLog)

        response = self.custom_payment_test(save_card = "n")
        self.assertTrue(len(models.PaymentLog.query.all()) == 1)
        card = models.Card.query.first()
        self.assertTrue(card != None)
        self.assertTrue(self.check_card_structure(card))
        self.assertTrue(card.user_saved == True)
        self.delete_all(models.Card)
        self.delete_all(models.PaymentLog)
        
        response = self.custom_payment_test(save_card = None)
        self.assertTrue(len(models.PaymentLog.query.all()) == 1)
        card = models.Card.query.first()
        self.assertTrue(card != None)
        self.assertTrue(self.check_card_structure(card))
        self.assertTrue(card.user_saved == False)
        self.delete_all(models.Card)
        self.delete_all(models.PaymentLog)

        response = self.custom_payment_test(save_card = "")
        self.assertTrue(len(models.PaymentLog.query.all()) == 1)
        card = models.Card.query.first()
        self.assertTrue(card != None)
        self.assertTrue(self.check_card_structure(card))
        self.assertTrue(card.user_saved == False)
        self.delete_all(models.Card)
        self.delete_all(models.PaymentLog)

    # test card deletion success
    @login_test
    def test_card_delete_success(self):
        self.custom_payment_test(save_card = "y")

        card = models.Card.query.first()

        # correct form submission
        response = self.client.delete(
            url_for("pay_bp.delete_card", card_id = card.id),
            follow_redirects=False
        )
        self.assertTrue(response.status_code == 200)

    # test card deletion incorrect card
    @login_test
    def test_card_delete_incorrect_card(self):
        self.custom_payment_test(save_card = "y")

        card = models.Card.query.first()

        # incorrect form submissions
        response = self.client.delete(
            url_for("pay_bp.delete_card", card_id = 99),
            follow_redirects=False
        )
        self.assertTrue(response.status_code == 404)

    # test card deletion unathorised user
    @login_test
    def test_card_delete_unauthorised_user(self):
        self.custom_payment_test(save_card = "y")

        card = models.Card.query.first()

        self.register("User", "Two", "user2@email.com", "aaaaaa", "01234567890", "2000-04-27")
        self.logout()
        self.login("user2@email.com", "aaaaaa")

        # incorrect form submissions
        response = self.client.delete(
            url_for("pay_bp.delete_card", card_id = 1),
            follow_redirects=False
        )
        self.assertTrue(response.status_code == 403)

    # test payment log
    @login_test
    def test_log_structure(self):
        # set up the payment
        desc = "Test Payment"
        price = 2.99
        self.create_test_payment(desc, price, "test", "test")

        # send the payments form
        response = self.client.post(
            url_for("pay_bp.pay"),
            data=dict(
                name_on_card="Test Name",
                card_number="4111111111111111",
                expiry_date="07/22",
                security_code="123",
                save_card="n",
                submit="Submit"
            ),
            follow_redirects=False
        )

        # check that the log exists
        logs = models.PaymentLog.query.all()
        self.assertTrue(len(logs) == 1)

        # check the log structure
        log = logs[0]
        self.assertTrue(log.user.id == current_user.id)
        self.assertTrue(log.card.user.id == current_user.id)
        self.assertTrue(isinstance(log.time, datetime.datetime))
        self.assertTrue(log.description == desc)
        self.assertTrue(log.get_price_decimal() == price)


    # test that the callback is correctly executed
    @login_test
    def test_callback(self):
        count_before = len(models.Membership.query.all())

        # set up the payment
        self.create_test_payment("Test Payment", 2.99, "test", "test")

        # send the payments form
        response = self.client.post(
            url_for("pay_bp.pay"),
            data=dict(
                name_on_card="Test Name",
                card_number="4111111111111111",
                expiry_date="07/22",
                security_code="123",
                save_card="n",
                submit="Submit"
            ),
            follow_redirects=False
        )

        count_after = len(models.Membership.query.all())
        self.assertTrue(count_before == count_after - 1)

    # test that the redirect works correctly
    @login_test
    def test_redirection(self):
        # set up the payment
        self.create_test_payment("Test Payment", 2.99, "test", "test")

        # send the payments form
        response = self.client.post(
            url_for("pay_bp.pay"),
            data=dict(
                name_on_card="Test Name",
                card_number="4111111111111111",
                expiry_date="07/22",
                security_code="123",
                save_card="n",
                submit="Submit"
            ),
            follow_redirects=False
        )

        # check that redirection was successful
        self.assertTrue(response.status_code == 302)
        self.assertTrue(response.location.endswith(url_for(redirections["test"])))

if __name__ == "__main__":
    unittest.main()