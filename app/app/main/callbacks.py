from datetime import datetime
from app.models import Booking
from app import db
from app.user.views import booking_email_confirmation

# adds a booking to the booking table for a user & activity
def create_booking(data, payment_card):
    user_id = data["user_id"]
    activity_id = data["activity_id"]
    booking = Booking(
        user_id=user_id, activity_id=activity_id, time_of_booking=datetime.now()
    )
    db.session.add(booking)
    db.session.commit()

    # send email
    booking_email_confirmation(booking)

    # generate receipt
