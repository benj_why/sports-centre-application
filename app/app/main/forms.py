from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SelectMultipleField, widgets
from wtforms.validators import DataRequired, Length, Email
from wtforms.fields.html5 import DateField, EmailField
from wtforms.fields import SelectField, SubmitField
from datetime import datetime


class Signup(FlaskForm):
    first_name = StringField("First Name", validators=[DataRequired()])
    last_name = StringField("Last Name", validators=[DataRequired()])
    email = EmailField("Email", validators=[DataRequired(), Email()])
    password = PasswordField("Password", validators=[DataRequired(), Length(6, 16)])
    emergency_number = StringField(
        "Emergency Phone Number", validators=[DataRequired(), Length(11, 11)]
    )
    dob = DateField("Date of Birth", validators=[DataRequired()])


class MultiCheckboxField(SelectMultipleField):
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()


# Form for selecting options for timetable display (range of dates, facility options)


class TimetableOptions(FlaskForm):
    facility = MultiCheckboxField(
        "",
        choices=[
            ("Swimming Pool", "Swimming Pool"),
            ("Squash Court 1", "Squash Court 1"),
            ("Squash Court 2", "Squash Court 2"),
            ("Squash Court 3", "Squash Court 3"),
            ("Squash Court 4", "Squash Court 4"),
            ("Fitness Room", "Fitness Room"),
            ("Sports Hall", "Sports Hall"),
        ],
        default=[
            "Swimming Pool",
            "Squash Court 1",
            "Squash Court 2",
            "Squash Court 3",
            "Squash Court 4",
            "Fitness Room",
            "Sports Hall",
        ],
    )
    start_time = SelectField("", coerce=int)
    end_time = SelectField("", coerce=int)
    start_date = SelectField("")


class BookingForm(FlaskForm):
    submit = SubmitField("Book Now", validators=[DataRequired()])


class CancelForm(FlaskForm):
    submit = SubmitField("Cancel Booking", validators=[DataRequired()])
