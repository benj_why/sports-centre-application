from flask import (
    Blueprint,
    render_template,
    redirect,
    url_for,
    jsonify,
    session,
    request,
)
from flask import current_app as app
from flask import abort
from flask_login import current_user, login_user, logout_user, login_required
from app.main.forms import Signup, TimetableOptions, BookingForm, CancelForm
from app import db, bcrypt
from app.models import User, Activity, Facility, Booking
from datetime import datetime, timedelta, date, time
from sqlalchemy import extract, and_
from app.user.views import booking_email_confirmation
import os
import html
import decimal
import re
from app.pay.payments import create_pending_payment
from app.main.callbacks import create_booking as booking_callback
from app.pay.payments import redirections as pay_redirects
from .helpers import get_activity_price


# Blueprint Configuration
main_bp = Blueprint(
    "main_bp", __name__, template_folder="templates", static_folder="static"
)


@main_bp.route("/index", methods=["GET"])
@main_bp.route("/", methods=["GET"])
def index():
    """Homepage route."""
    images = os.listdir(os.path.join(app.static_folder, "CarouselImages"))
    return render_template("index.html", images=images)


@main_bp.route("/facility/<facility_choice>")
def facility(facility_choice):
    facility_name_list = re.findall("[A-Z][^A-Z]*", facility_choice)
    facility_name = ""
    for i in facility_name_list:
        facility_name = facility_name + i + " "
    facility_name = facility_name.strip()
    if facility_name == "Squash Courts":
        facility_name = "Squash Court 1"
    if db.session.query(Facility.name).filter_by(name=facility_name).scalar() is None:
        return abort(404)
    imageFolder = facility_choice + "Images"
    folder = os.listdir(os.path.join(app.static_folder, imageFolder))

    facility_id = Facility.query.filter_by(name=facility_name).first().id
    activities = (
        Activity.query.filter_by(facility_id=facility_id).group_by(Activity.name).all()
    )
    for i in range(0, len(activities)):
        activities[i] = activities[i].name

    activity_list = ",".join(activities)

    return render_template(
        "facility.html",
        folder=folder,
        folder_name=imageFolder,
        facility_name=facility_name,
        activity_list=activity_list,
    )


@main_bp.route("/signup", methods=["GET", "POST"])
def signup():
    signup_form = Signup()
    if signup_form.validate_on_submit():
        create_user(
            signup_form.first_name.data,
            signup_form.last_name.data,
            signup_form.email.data,
            signup_form.password.data,
            signup_form.emergency_number.data,
            signup_form.dob.data,
        )
        return redirect(url_for("main_bp.index"))
    return render_template("signup.html", form=signup_form)


def create_user(first_name, last_name, email, password, emergency_num, dob):
    # create new user, hash password and add to the database
    password_hash = bcrypt.generate_password_hash(password).decode("utf-8")
    new_user = User(
        first_name=first_name,
        last_name=last_name,
        email=email,
        password=password_hash,
        emergency_number=emergency_num,
        dob=dob,
        user_type=3,
    )
    db.session.add(new_user)
    db.session.commit()


# Main code ran when on timetable page


@main_bp.route("/timetable", methods=["GET", "POST"])
def timetable():

    # Sets current day and creates list of days in upcoming month

    today = datetime.now()
    upcoming_month = []
    times = [
        (900, "9:00"),
        (1000, "10:00"),
        (1100, "11:00"),
        (1200, "12:00"),
        (1300, "13:00"),
        (1400, "14:00"),
        (1500, "15:00"),
        (1600, "16:00"),
        (1700, "17:00"),
    ]

    for i in range(0, 28):
        upcoming_month.append(today + timedelta(days=i))

    # Creates instance of the timetable options form
    # Sets the range of choices for start time, end time, and start date

    options = TimetableOptions()

    options.start_time.choices = times[0:8]
    options.end_time.choices = times[1:9]

    options.start_date.choices = [
        (day.strftime("%x"), day.strftime("%d/%m/%Y")) for day in upcoming_month
    ]

    # After submitting the timetable options, generate timetable function is run

    if options.validate_on_submit():
        return generateTimetable(options)

    # Loads the html page, using the options form created

    facilities = Facility.query.all()

    return render_template("timetable.html", form=options, facilities=facilities)


def generateTimetable(options):

    # Reads completed form data
    start_time = options.start_time.data
    end_time = options.end_time.data
    selected_day = datetime.strptime(options.start_date.data, "%x")
    activities = [[], [], [], [], [], [], []]
    next_week = []

    facilities = options.facility.data

    if len(facilities) > 0:
        for i in range(0, len(facilities)):
            facilities[i] = Facility.query.filter_by(name=facilities[i]).first().id

    times = [
        (900, "9:00"),
        (1000, "10:00"),
        (1100, "11:00"),
        (1200, "12:00"),
        (1300, "13:00"),
        (1400, "14:00"),
        (1500, "15:00"),
        (1600, "16:00"),
        (1700, "17:00"),
    ]

    # Finds index position of start and end time in times list

    for time in range(0, len(times)):
        if times[time][0] == start_time:
            start_index = time
        elif times[time][0] == end_time:
            end_index = time
    times = times[start_index:end_index]

    # Loops through all 7 days in the upcoming week, and queries activities
    """for row in Activity.query.all():
        if(row.facility.name == "Fitness Room"):
            print("aight")"""
    for i in range(0, 7):
        # Creates next week list, and sets current date on each loop
        next_week.append(selected_day + timedelta(days=i))
        current_date = selected_day + timedelta(days=i)
        # Iterated through selected hours in the day
        for j in range(0, int(((end_time - start_time) / 100))):
            # Queries activities based on current day, hour, and selected facility
            """if facilities == []:
                activities_in_hour = Activity.query.filter(
                    and_(
                        extract("hour", Activity.start_time)
                        == int((start_time / 100) + j)
                    ),
                    (extract("month", Activity.start_time) == current_date.month),
                    (extract("day", Activity.start_time) == current_date.day),
                ).all()
            else:"""

            activities_in_hour = Activity.query.filter(
                and_(
                    (extract("hour", Activity.start_time) == int(start_time / 100) + j),
                    (extract("month", Activity.start_time) == current_date.month),
                    (extract("day", Activity.start_time) == current_date.day),
                    Activity.facility_id.in_(facilities),
                )
            ).all()
            # Appends the activities queried to the activities list
            activities[i].append(activities_in_hour)

    facilities = Facility.query.all()

    # Renders the HTML page, using parameters for options, the upcoming week, times, and activities
    return render_template(
        "timetable.html",
        form=options,
        week=next_week,
        activities=activities,
        times=times,
        facilities=facilities,
    )


@main_bp.route("/timetable/<start_time>")

# Function used in the rendering of a dynamic select field for times


def endTime(start_time):

    time_array = [
        (900, "9:00"),
        (1000, "10:00"),
        (1100, "11:00"),
        (1200, "12:00"),
        (1300, "13:00"),
        (1400, "14:00"),
        (1500, "15:00"),
        (1600, "16:00"),
        (1700, "17:00"),
    ]

    return jsonify({"Times": time_array})


@main_bp.route("/activities/<activity_id>", methods=["GET", "POST"])
def activity(activity_id):
    activity = Activity.query.filter_by(id=activity_id).first()
    facility_name = activity.facility.name
    # facility_name = Facility.query.filter_by(id=activity.facility_id).first().name

    capacity = {}
    capacity["initial"] = (
        Facility.query.filter_by(id=activity.facility_id).first().capacity
    )
    capacity["booked"] = Booking.query.filter_by(activity_id=activity.id).count()
    capacity["remaining"] = capacity["initial"] - capacity["booked"]
    capacity["percentage_remaining"] = (
        capacity["remaining"] / capacity["initial"]
    ) * 100

    booking = None
    if current_user.is_authenticated:
        booking = Booking.query.filter_by(
            activity_id=activity.id, user_id=current_user.id
        ).first()

    price, price_text, peak_time = get_activity_price(activity)

    form = BookingForm()
    if form.validate_on_submit():
        # create a pending payment
        data = {"activity_id": activity_id, "user_id": current_user.get_id()}

        if price == 0:
            # skip payment and go straight to callback and redirect
            booking_callback(data, None)
            return redirect("/pay/thankyou")
        else:
            desc = "Activity Booking: " + activity.name + " at " + facility_name
            pending_payment = create_pending_payment(
                current_user.get_id(), price, desc, data, "booking", "booking"
            )

            # assign pending payment to user session
            session["pending_payment"] = pending_payment.id

            # redirect the user for payment
            return redirect(url_for("pay_bp.pay"))

    cancel_form = CancelForm()
    if cancel_form.validate_on_submit():
        cancel_booking(current_user.id, activity_id)

        # redirect the user back to the activity page
        return redirect(url_for("pay_bp.pay"))

    return render_template(
        "activity.html",
        facility_name=facility_name,
        activity=activity,
        capacity=capacity,
        price_text=price_text,
        booking=booking,
        peak_time=peak_time,
        user=current_user,
        current_hour=datetime.now(),
        form=form,
        cancel_form=cancel_form,
    )


def cancel_booking(user_id, activity_id):
    Booking.query.filter_by(user_id=user_id, activity_id=activity_id).delete()
    db.session.commit()


@main_bp.route("/activities", methods=["GET"])
def activities_timetable():
    # for each timeslot, get all the activities
    facilities = Facility.query.all()

    # work out the facility filter to apply
    filtered_facilities = []
    facility_args = request.args.get("facilities")
    if facility_args:
        print(facility_args)
        facility_filter = facility_args.split(",")
        for facility in facilities:
            if str(facility.id) in facility_filter:
                filtered_facilities.append(facility.id)
    else:
        filtered_facilities = [facility.id for facility in facilities]

    # apply the facilities filter
    filter_activities = Activity.query.filter(
        Activity.facility_id.in_(filtered_facilities)
    )
    """
    filter_activities = None
    if request.method == 'POST':
        filter_activities = []
        for facility in facilities:
            if request.values.get(facility.name):
                filter_activities.append(facility)
    else:
        filter_activities = [facility for facility in facilities]


    activities = Activity.query
    activities.filter(Activity.facility.name in filter_facilities)"""

    dates = []

    # get all the activities in the time frame
    base = date.today()
    dates = [{"date": base + timedelta(days=x), "activities": []} for x in range(7)]
    for activity_date in dates:
        activity_date["activities"] = filter_activities.filter(
            (extract("year", Activity.start_time) == activity_date["date"].year),
            (extract("month", Activity.start_time) == activity_date["date"].month),
            (extract("day", Activity.start_time) == activity_date["date"].day),
        ).all()

    return render_template(
        "activities_timetable.html",
        dates=dates,
        facilities=facilities,
        filtered_facilities=filtered_facilities,
    )
