from flask_login import current_user


def get_activity_price(activity):
    peak_time = False
    price = activity.price
    if (
        activity.start_time.strftime("%A") == "Saturday"
        or activity.start_time.strftime("%A") == "Sunday"
        or (
            activity.start_time.strftime("%-H") >= "15"
            and activity.start_time.strftime("%-H") < "17"
        )
    ):
        peak_time = True
        price = price * 1.5

    if current_user.is_authenticated and current_user.is_member():
        price = 0

    if price == 0:
        price_text = "Free!"
    else:
        price_text = "£{0:.2f}".format(price)

    return price, price_text, peak_time
