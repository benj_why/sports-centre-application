import sqlite3
from datetime import datetime, timedelta
from flask_bcrypt import Bcrypt
import random
import string

bcrypt = Bcrypt()

# ----------------------------------DATA-----------------------------------------#
# Users (us)
names = [
    "JJ Style",
    "Sarah Lewis",
    "Milo Carroll",
    "Benjy Waldman",
    "Lucas Gurney",
    "Miles Pulley",
    "Nonmember Email",
]
users = []
for i in range(len(names)):
    name = names[i]
    split = name.split(" ")
    users.append(
        (
            split[0],
            split[1],
            bcrypt.generate_password_hash(split[1].lower()),
            "0" * 11,
            "{}@{}.com".format(split[0].lower(), split[1].lower()),
            3,
            datetime.now(),
        )
    )
employee = (
    "emp_fname",
    "emp_sname",
    bcrypt.generate_password_hash("employee"),
    "0" * 11,
    "employee@email.com",
    2,
    datetime.now(),
)
manager = (
    "man_fname",
    "man_sname",
    bcrypt.generate_password_hash("manager"),
    "0" * 11,
    "manager@email.com",
    1,
    datetime.now(),
)
users.extend((employee, manager))

# Other users (can't login with them as not hashing passwords as takes ages)
NUM_USERS = 100
first_names, last_names = [], []
with open("data/first-names.txt", "r") as fnames:
    for row in fnames:
        first_names.append(row.strip().capitalize())
with open("data/last-names.txt", "r") as lnames:
    for row in lnames:
        last_names.append(row.strip().capitalize())
for i in range(NUM_USERS):
    fname = random.choice(first_names)
    lname = random.choice(last_names)
    users.append(
        (
            fname,
            lname,
            lname,
            "0" * 11,
            "{}@{}.com".format(fname.lower(), lname.lower()),
            3,
            datetime.now(),
        )
    )

# Facilities
facilities = [("Swimming Pool", 32), ("Fitness Room", 25), ("Sports Hall", 20)]
facilities.extend([("Squash Court {}".format(i), 4) for i in range(1, 5)])
act_types = [
    ("General Use"),
    ("Lessons"),
    ("Lane Swimming"),
    ("Team events"),
    ("General Session"),
]
activity_types = [
    ("Swimming Pool", "General Use"),
    ("Swimming Pool", "Lessons"),
    ("Swimming Pool", "Team events"),
    ("Swimming Pool", "Lane Swimming"),
    ("Fitness Room", "General Use"),
    ("Squash Court", "Team events"),
    ("Squash Court", "General Session"),
    ("Sports Hall", "General Session"),
]

# ------------------------------------CONNECT TO DATABASE-----------------------------------#
db = None
try:
    db = sqlite3.connect("app.db")
except Exception as e:
    print(e.args[0])

# -----------------------------------INSERT DATA-----------------------------------#
if db:
    cursor = db.cursor()

    # Delete rows in tables
    delete_tables = [
        "User",
        "Membership",
        "Facility",
        "Activity",
        "Booking",
        "activity_type",
        "membership_renewal",
    ]

    for table in delete_tables:
        cursor.execute("DELETE FROM " + table)

    # Insert users
    user_sql = """INSERT INTO
                    User(first_name,last_name,password,emergency_number,email,user_type,dob)
                    VALUES(?,?,?,?,?,?,?)
                """
    for user in users:
        cursor.execute(user_sql, user)

    # Insert members
    rows = cursor.execute("SELECT * FROM User WHERE user_type=3")
    members = []
    for row in rows:
        if row[1] != "Nonmember":  # special non-member account for testing
            now = datetime.now()
            members.append((now, now.replace(year=now.year + 1), row[0]))
    random.shuffle(members)
    members = members[: len(members) // 2]
    for member in members:
        cursor.execute(
            "INSERT INTO Membership(start_date,end_date,user_id) VALUES(?,?,?)", member
        )

    # Insert Cards for membership renewals
    random.shuffle(members)
    members_renew = members[: len(members) // 2]
    for i in range(len(members_renew)):
        cursor.execute(
            """ INSERT INTO Card(user_id,card_number,expiry_date,address,postcode,name_on_card)
                VALUES(?,?,?,?,?,?)""",
            (
                members_renew[i][1],
                "4" + ("1" * 15),
                datetime.now() + timedelta(days=300),
                "address",
                "postcode",
                "name",
            ),
        )

    cards = []
    for row in cursor.execute("SELECT * FROM Card"):
        cards.append(row)

    for i in range(len(members_renew)):
        cursor.execute(
            """ INSERT INTO membership_renewal(user_id,card_id,renew_on,renew_type)
                VALUES(?,?,?,?)""",
            (
                members_renew[i][2],
                cards[i][0],
                datetime.now() + timedelta(days=random.randint(10, 100)),
                random.choice(["m", "y"]),
            ),
        )

    # Inserting activity type
    type_sql = """INSERT INTO
                    activity_type(id, name)
                    VALUES (?,?)
                """
    i = 1
    for types in act_types:
        cursor.execute(type_sql, (i, types))
        i += 1

    # inserting facilties
    for facility in facilities:
        cursor.execute("INSERT INTO Facility(name, capacity) VALUES(?,?)", facility)

    # connect facilty to acivty types
    act_types = cursor.execute("SELECT * FROM activity_type")
    mapping = []
    for t in act_types:
        mapping.append((t[0], t[1]))

    for types in activity_types:
        rows = cursor.execute("SELECT * FROM Facility")
        for row in rows:
            if types[0] in row[1]:
                for t in mapping:
                    if t[1] == types[1]:
                        cursor.execute(
                            "INSERT INTO facility_activity_type(facility_id,type_id) VALUES(?,?)",
                            (row[0], t[0]),
                        )

    BACKDATE = 365
    ADVANCE = 30

    # --------------------------- adding  activities =   SWIMMING POOL----------------------

    facility_pool = []
    rows = cursor.execute("SELECT * FROM Facility WHERE name='Swimming Pool'")
    for row in rows:
        facility_pool.append(row)

    # booking activites for last year and a month
    day = datetime.today() - timedelta(days=BACKDATE)
    for i in range(BACKDATE + ADVANCE):
        day = day.replace(hour=9, minute=0)  # 9am
        for i in range(9):
            new_day = day + timedelta(hours=1)  # each hour in the day
            for pool in facility_pool:
                activity, price = None, None
                if (day.weekday() == 0 and (day.hour == 9 or day.hour == 10)) or (
                    day.weekday() == 2 and day.hour in range(15, 17)
                ):
                    activity = "Lane Swimming"
                    price = 3.5
                elif day.weekday() == 1 and (day.hour == 12 or day.hour == 13):
                    activity = "Lessons"
                    price = 4.5
                elif day.weekday() == 2 and (day.hour in range(12, 15)):
                    activity = "Team Events"
                    price = 3.75
                elif day.weekday() in [2, 4] and day.hour in range(9, 11):
                    activity = "General Use"
                    price = 3.0
                if activity != None and price != None:
                    cursor.execute(
                        """INSERT INTO Activity(name, start_time, end_time, price, facility_id)
                    VALUES(?,?,?,?,?)""",
                        (activity, day, new_day, price, pool[0]),
                    )
            day = new_day
        day += timedelta(days=1)

    # --------------------------- adding  activities =   Fitness Room----------------------

    facility_gym = []
    rows = cursor.execute("SELECT * FROM Facility WHERE name='Fitness Room'")
    for row in rows:
        facility_gym.append(row)

    day = datetime.today() - timedelta(days=BACKDATE)
    for i in range(BACKDATE + ADVANCE):
        day = day.replace(hour=9, minute=0)  # 9am
        for i in range(9):
            new_day = day + timedelta(hours=1)  # each hour in the day
            for gym in facility_gym:
                activity, price = None, None
                if (day.weekday() == 0 and (day.hour in range(10, 13))) or (
                    day.weekday() in [2, 3, 4] and day.hour in range(12, 15)
                ):
                    activity = "General Use"
                    price = 2.25
                if activity != None and price != None:
                    cursor.execute(
                        """INSERT INTO Activity(name, start_time, end_time, price, facility_id)
                    VALUES(?,?,?,?,?)""",
                        (activity, day, new_day, price, gym[0]),
                    )
            day = new_day
        day += timedelta(days=1)

    # --------------------------- adding  activities =   Squash Court %  ----------------------

    squash_courts = []
    rows = cursor.execute("SELECT * FROM Facility WHERE name LIKE 'Squash Court %'")
    for row in rows:
        squash_courts.append(row)

    day = datetime.today() - timedelta(days=BACKDATE)
    for i in range(BACKDATE + ADVANCE):
        day = day.replace(hour=9, minute=0)  # 9am
        for i in range(9):
            new_day = day + timedelta(hours=1)  # each hour in the day
            for court in squash_courts:
                activity, price = None, None
                if day.weekday() in [1, 3] and day.hour in [10, 12, 13, 16]:
                    activity = "General Session"
                    price = 3.0
                elif day.weekday() == 0 and day.hour in range(14, 16):
                    activity = "Team Events"
                    price = 2.5
                if activity != None and price != None:
                    cursor.execute(
                        """INSERT INTO Activity(name, start_time, end_time, price, facility_id)
                    VALUES(?,?,?,?,?)""",
                        (activity, day, new_day, price, court[0]),
                    )
            day = new_day
        day += timedelta(days=1)

    # --------------------------- user BOOKINGS ----------------------------

    users = []
    user_row = cursor.execute("SELECT * FROM User")
    for row in user_row:
        users.append(row)

    activities = []
    activity_rows = cursor.execute("SELECT * FROM Activity")
    for row in user_row:
        activities.append(row)

    for act in activities:
        facilities_with_id = list(
            cursor.execute("SELECT * FROM Facility WHERE id={}".format(act[5]))
        )
        facility = random.choice(facilities_with_id)
        max_capacity = facility[2]
        inserted_users = []
        for i in range(random.randint(1, max_capacity)):
            valid_user = False
            while not valid_user:
                user = random.choice(users)
                valid_user = user not in inserted_users
            inserted_users.append(user)
            cursor.execute(
                """ INSERT INTO Booking(user_id, activity_id, time_of_booking)
                    VALUES(?,?,?)""",
                (user[0], act[0], datetime.now()),
            )

    db.commit()
