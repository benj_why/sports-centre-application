import os
import unittest
import sys

topdir = os.path.join(os.path.dirname(__file__), "..")
sys.path.append(topdir)

from app import create_app, db

from datetime import datetime, timedelta, date
from config import basedir
from app.models import User, Booking, Facility, Activity
from flask import Flask, url_for, request, jsonify
from flask import json

from app.user.views import create_booking

import logging

logging.getLogger("weasyprint").setLevel(100)


class TestReceipts(unittest.TestCase):

    ## set up ##
    ## the same for all test files ##
    @classmethod
    def setUpClass(cls):
        cls.do()

    @classmethod
    def do(self):
        app = create_app()
        app.config["TESTING"] = True
        app.config["WTF_CSRF_ENABLED"] = False
        app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(
            basedir, "test.db"
        )
        self.app = app.test_client()
        db.app = app
        db.create_all()

    def setUp(self):
        print("testing receipt methods")
        self.create_user("vito", "corleone", "password")
        self.create_user("michael", "corleone", "password")
        self.create_facility("pool", 24)
        self.create_activity(
            "lane swimming",
            datetime(2020, 4, 15, 9, 0),
            datetime(2020, 4, 15, 10, 0),
            1,
            2.5,
        )

    @classmethod
    def tearDownClass(cls):
        db.session.remove()
        db.drop_all()

    def tearDown(self):
        self.logout()
        db.session.query(User).delete()
        db.session.query(Activity).delete()
        db.session.query(Booking).delete()
        db.session.query(Facility).delete()
        db.session.commit()

    ## helpers ##

    def reset(self):
        db.session.query(User).delete()
        db.session.query(Activity).delete()
        db.session.query(Booking).delete()
        db.session.query(Facility).delete()
        db.session.commit()

    def create_user(self, fname, lname, pwd):
        user = User(
            first_name=fname,
            last_name=lname,
            email="{}@{}.com".format(fname, lname),
            user_type=3,
            dob=datetime.now(),
            emergency_number="0" * 11,
        )
        user.set_password(pwd)
        db.session.add(user)
        db.session.commit()

    def create_facility(self, name, capacity):
        facility = Facility(name=name, capacity=capacity)
        db.session.add(facility)
        db.session.commit()

    def create_activity(self, name, start, end, facility_id, price):
        activity = Activity(
            name=name,
            start_time=start,
            end_time=end,
            facility_id=facility_id,
            price=price,
        )
        db.session.add(activity)
        db.session.commit()

    def login(self, email, password):
        return self.app.post("/user/login", data=dict(email=email, password=password))

    def logout(self):
        return self.app.get("/user/logout")

    ## tests ##

    # ## test create booking ## #
    def test_create_booking(self):
        user = User.query.all()[0]
        activity = Activity.query.all()[0]

        create_booking(user.id, activity.id)

        bookings = Booking.query.all()
        self.assertEqual(len(bookings), 1)

        booking = bookings[0]
        self.assertEqual(User.query.get(booking.user_id), user)
        self.assertEqual(Activity.query.get(booking.activity_id), activity)

    def test_valid_receipt(self):
        user = User.query.all()[0]
        activity = Activity.query.all()[0]
        db.session.refresh(activity)
        db.session.expunge(activity)
        create_booking(user.id, activity.id)

        booking = Booking.query.filter_by(
            user_id=user.id, activity_id=activity.id
        ).first()
        self.login(user.email, "password")
        response = self.app.get(
            "/user/bookings/{}/receipt?booking_id={}".format(booking.id, booking.id)
        )
        self.assertEqual(response.status_code, 200)

        self.logout()
        self.reset()

    def test_invalid_receipt_access(self):
        user1 = User.query.all()[0]
        activity = Activity.query.all()[0]
        db.session.refresh(activity)
        db.session.expunge(activity)
        create_booking(user1.id, activity.id)
        booking = Booking.query.filter_by(
            user_id=user1.id, activity_id=activity.id
        ).first()

        user2 = User.query.all()[1]
        self.login(user2.email, "password")
        response = self.app.get(
            "/user/bookings/{}/receipt?booking_id={}".format(booking.id, booking.id)
        )
        self.assertRegex(str(response), "4\d\d")
        self.logout()
        self.reset()

    def test_valid_receipt_pdf(self):
        user = User.query.all()[0]
        activity = Activity.query.all()[0]
        db.session.refresh(activity)
        db.session.expunge(activity)
        create_booking(user.id, activity.id)
        booking = Booking.query.filter_by(
            user_id=user.id, activity_id=activity.id
        ).first()

        self.login(user.email, "password")
        response = self.app.get(
            "/user/bookings/{}/receipt_pdf?booking_id={}".format(booking.id, booking.id)
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.mimetype, "application/pdf")

        self.logout()
        self.reset()

    def test_invalid_receipt_not_exist(self):
        user = User.query.all()[0]
        activity = Activity.query.all()[0]
        db.session.refresh(activity)
        db.session.expunge(activity)
        create_booking(user.id, activity.id)
        booking = Booking.query.filter_by(
            user_id=user.id, activity_id=activity.id
        ).first()

        self.login(user.email, "password")
        response = self.app.get(
            "/user/bookings/{}/receipt_pdf?booking_id={}".format(
                booking.id + 100, booking.id + 100
            )
        )
        self.assertRegex(str(response), "4\d\d")

        self.logout()
        self.reset()


if __name__ == "__main__":
    unittest.main()  #
