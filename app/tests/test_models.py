import os
import unittest
import sys
## topdir must be set before importing the models
topdir = os.path.join(os.path.dirname(__file__), "..")
sys.path.append(topdir)

from datetime import datetime, timedelta
from flask import json
from flask import Flask, url_for, request, jsonify
from app.models import User, Activity, ActivityType, Booking, Card, Facility
from config import basedir
from app import create_app, db




class TestModels(unittest.TestCase):

    ## set up ##
    ## the same for all test files ##
    @classmethod
    def setUpClass(cls):
        cls.do()

    @classmethod
    def do(self):
        app = create_app()
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(
            basedir, 'test.db')
        self.app = app
        db.app = app
        db.create_all()

    def setUp(self):
        print("testing")
        
    @classmethod
    def tearDownClass(cls):
        db.session.remove()
        db.drop_all()

    def tearDown(self):
        db.session.query(User).delete()
        db.session.query(Activity).delete()
        db.session.query(Booking).delete()
        db.session.query(Card).delete()
        db.session.query(Facility).delete()
        db.session.commit()

    ## my helper functions ##

    def reset(self):
        db.session.query(User).delete()
        db.session.commit()

    
    def create_user(self, first_name, last_name, email, password):
        user = User(first_name=first_name, last_name=last_name, email=email)
        user.set_password(password)
        db.session.add(user)
        db.session.commit()

    def create_card(self,user, card_number, expiry_date, address, postcode, name_on_card, membership_renewals):
        card = Card(user=user,
                    card_number=card_number,
                    expiry_date=expiry_date, 
                    address=address, 
                    postcode=postcode, 
                    name_on_card=name_on_card,
                    membership_renewals=membership_renewals)
        db.session.add(card)
        db.session.commit()

    def create_facility(self, name, capacity):
        facility = Facility(name=name, capacity=capacity)
        db.session.add(facility)
        db.session.commit()

    def create_activity(self, name, start, end, facility):
        activity = Activity(name=name, start_time=start, end_time=end, facility=facility)
        db.session.add(activity)
        db.session.commit()

    def create_activity_type(self, name):
        activity_type = ActivityType(name=name)
        db.session.add(activity_type)
        db.session.commit()

    def create_booking(self,activity, user, time):
        booking = Booking(user=user,activity=activity, time_of_booking=time)
        db.session.add(booking)
        db.session.commit()



    ## tests start ##

    """ def test_valid_user_creation(self):
        self.create_user("milo", "carroll", "sc19m2c@leeds.uk", "aPassword")
        user = User.query.filter_by(email="sc19m2c@leeds.uk").first()
        self.assertEqual(user.first_name, "milo")
        self.assertEqual(user.last_name, "carroll")
        self.assertEqual(user.email, "sc19m2c@leeds.uk")
        self.assertEqual(user.check_password("aPassword"), True)
        self.reset()

    def test_valid_card_creation(self):
        #create date
        expiry = datetime.now()
        #create card
        self.create_user("milo", "carroll", "sc19m2c@leeds.uk", "aPassword")
        user = User.query.filter_by(email="sc19m2c@leeds.uk").first()
        #check card
        self.create_card(user, "111", expiry, "address_line_1", "postcode1", "milo_carroll", [] )
        card = Card.query.all()[0]
        self.assertEqual(card.card_number, "111")
        self.assertEqual(card.expiry_date, expiry)
        self.assertEqual(card.address, "address_line_1")
        self.assertEqual(card.postcode, "postcode1")
        self.assertEqual(card.name_on_card, "milo_carroll")
        self.assertEqual(card.membership_renewals.all(), [])
        #cheeck user
        user = User.query.filter_by(email="sc19m2c@leeds.uk").first()
        self.assertEqual(user.cards.all()[0].id, card.id)


    def test_valid_facility_creation(self):
        self.create_facility("facilty1", 200)
        facility = Facility.query.filter_by(name="facilty1").first()
        self.assertEqual(facility.name, "facilty1")
        self.assertEqual(facility.capacity, 200)
        self.reset()

    def test_valid_activity_creation(self):
        #set up times for activity
        now = datetime.now() 
        later = datetime.now() + timedelta(hours=1)
        #create facilty for activity and activity
        self.create_facility("facilty1", 200)
        facility = Facility.query.filter_by(name="facilty1").first()
        self.create_activity("activity1", now, later, facility)
        #check activty variables
        activity = Activity.query.filter_by(name="activity1").first()
        self.assertEqual(activity.name, "activity1")
        self.assertEqual(activity.start_time, now)
        self.assertEqual(activity.end_time, later)
        self.assertEqual(activity.facility.name, facility.name)
        #check facility is storeing activities
        facility = Facility.query.filter_by(name="facilty1").first()
        self.assertEqual(facility.activities[0].name, "activity1")

        self.reset() """
    

    def test_valid_activity_type_creation(self):
        #set up times for activity
        now = datetime.now() 
        later = datetime.now() + timedelta(hours=1)
        #create facilty for activity and activity
        self.create_facility("facilty1", 200)
        facility = Facility.query.filter_by(name="facilty1").first()
        self.create_activity_type("activity_type_1")
        #check activty variables
        activity = ActivityType.query.filter_by(name="activity_type_1").first()
        print(activity)
        self.assertEqual(activity.name, "activity_type_1")

        facility.activity_types.append(activity)
        db.session.commit()

        self.reset()
    

    """ def test_valid_booking_creation(self):
        #set up times for activity
        test_time = datetime.now() - timedelta(hours=20)
        now = datetime.now() 
        later = datetime.now() + timedelta(hours=1)
        #create objects required for the creation of a booking
        self.create_user("milo", "carroll", "sc19m2c@leeds.uk", "aPassword")
        user = User.query.filter_by(email="sc19m2c@leeds.uk").first()
        self.create_facility("facilty1", 200)
        facility = Facility.query.filter_by(name="facilty1").first()
        self.create_activity("activity1", now, later, facility)
        activity = Activity.query.filter_by(name="activity1").first()
        #create booking
        self.create_booking(activity,user, test_time)
        booking = Booking.query.all()[0]
        #check booking
        self.assertEqual(booking.user.first_name, "milo")
        self.assertEqual(booking.activity.name, "activity1")
        #check user
        user = User.query.filter_by(email="sc19m2c@leeds.uk").first()
        self.assertEqual(user.bookings[0].time_of_booking, booking.time_of_booking )
        #check activity
        activity = Activity.query.filter_by(name="activity1").first()
        self.assertEqual(activity.users[0].time_of_booking, booking.time_of_booking )
        
        self.reset() """
    

    ####################################### REMAIN TESTS TO WRITE


    """def test_valid_booking_creation(self):
        self.create_user("milo", "carroll", "sc19m2c@leeds.uk", "aPassword")
        user = User.query.filter_by(email="sc19m2c@leeds.uk").first()
        self.assertEqual(user.first_name, "milo")
        self.assertEqual(user.last_name, "carroll")
        self.assertEqual(user.email, "sc19m2c@leeds.uk")
        self.assertEqual(user.check_password("aPassword"), True)
        self.reset() 

    def test_valid_booking_creation(self):
        self.create_user("milo", "carroll", "sc19m2c@leeds.uk", "aPassword")
        user = User.query.filter_by(email="sc19m2c@leeds.uk").first()
        self.assertEqual(user.first_name, "milo")
        self.assertEqual(user.last_name, "carroll")
        self.assertEqual(user.email, "sc19m2c@leeds.uk")
        self.assertEqual(user.check_password("aPassword"), True)
        self.reset() """


if __name__ == '__main__':
    unittest.main()
