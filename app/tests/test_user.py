import os
import unittest
import sys

topdir = os.path.join(os.path.dirname(__file__), "..")
sys.path.append(topdir)

from app import create_app, db

from datetime import datetime, timedelta, date
from config import basedir
from app.models import User
from flask import Flask, url_for, request, jsonify
from flask import json


class TestUsers(unittest.TestCase):

    ## set up ##
    ## the same for all test files ##
    @classmethod
    def setUpClass(cls):
        cls.do()

    @classmethod
    def do(self):
        app = create_app()
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(
            basedir, 'test.db')
        self.app = app.test_client()
        db.app = app
        db.create_all()

    def setUp(self):
        print("testing users")
        

    @classmethod
    def tearDownClass(cls):
        db.session.remove()
        db.drop_all()

    def tearDown(self):
        self.logout()
        db.session.query(User).delete()
        db.session.commit()

    ## helpers ##

    def reset(self):
        db.session.query(User).delete()
        db.session.commit()

    def create_user(self):
        user = User(first_name="milo", last_name="c", email="m@b.com", user_type=3, dob=datetime.now(), emergency_number="07987654321")
        user.set_password("FlaskApp")
        db.session.add(user)
        db.session.commit()

    def login(self, email, password):
        return self.app.post("/user/login", data=dict(email=email, password=password))

    def logout(self):
        return self.app.get("/user/logout")

    def register(self, first_name, last_name, email, password, number, dob):
        return self.app.post(
            "/user/signup",
            data=dict(
                first_name=first_name,
                last_name=last_name,
                email=email,
                password=password,
                emergency_number=number,
                dob=dob
            ),
        )
    
    def change_password(self, old_p, new_p, confirm_new_p):
        return self.app.post(
            "/user/changePassword",
            data=dict(
                password=old_p,
                new_password=new_p,
                new_password_confirm=confirm_new_p
            ),
        )

    def confirm_password(self, password):
        return self.app.post(
            "/user/confirmPassword",
            data=dict(
                password=password
            ),
        )

    def edit_user_details(self, first_name, last_name, email, emergency_number, dob):
        return self.app.post(
            "/user/account",
            data=dict(
                first_name=first_name,
                last_name=last_name,
                email=email,
                emergency_number=emergency_number,
                dob=dob
            ),
        )


   

    def register(self, first_name, last_name, email, password, number, dob):
        return self.app.post(
            "/user/signup",
            data=dict(
                first_name=first_name,
                last_name=last_name,
                email=email,
                password=password,
                emergency_number=number,
                dob=dob
            ),
        )
    
    def change_password(self, old_p, new_p, confirm_new_p):
        return self.app.post(
            "/user/changePassword",
            data=dict(
                password=old_p,
                new_password=new_p,
                new_password_confirm=confirm_new_p
            ),
        )

    def confirm_password(self, password):
        return self.app.post(
            "/user/confirmPassword",
            data=dict(
                password=password
            ),
        )

    def edit_user_details(self, first_name, last_name, email, emergency_number, dob):
        return self.app.post(
            "/user/account",
            data=dict(
                first_name=first_name,
                last_name=last_name,
                email=email,
                emergency_number=emergency_number,
                dob=dob
            ),
        )


   

    ## tests ##

    # registration

    def test_valid_user_registration(self):
        response = self.register(
            "milo", "carroll", "m@b.com", "FlaskApp", "07715354545", "1111-11-10"
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, "http://localhost/user/account")
        user = User.query.all()[0]
        self.assertEqual(user.first_name, "milo")
        response = self.app.get(response.location)
        self.reset()

    # ## test login ##

    def test_valid_user_login(self):
        self.create_user()
        response = self.login("m@b.com", "FlaskApp")
        self.assertEqual(response.status_code, 200)
        self.reset()

    def test_invalid_email_user_login(self):
        self.create_user()
        response = self.login("c@b.com", "FlaskApp")
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Email is incorrect.", response.data)
        self.reset()

    def test_invalid_password_user_login(self):
        self.create_user()
        response = self.login("m@b.com", "FlasssskApp")
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Password is incorrect.", response.data)
        self.reset()

    ## test logout ##

    def test_logout(self):
        self.create_user()
        response = self.login("m@b.com", "FlaskApp")
        response = self.logout()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, "http://localhost/")

    def test_logout_not_authenticated(self):
        response = self.logout()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, "http://localhost/")

    ## test change password


    def test_valid_change_password(self):
        self.create_user()
        response = self.login("m@b.com", "FlaskApp")
        response = self.change_password("FlaskApp", "newPassword", "newPassword")
        self.assertEqual(response.status_code,200)
        user = User.query.all()[0]
        self.assertTrue(user.check_password("newPassword"))
    
    def test_invalid_change_password(self):
        self.create_user()
        #check unauthenticated - should redirect
        response = self.change_password("FlaskApp", "incorrect", "newPassword")
        self.assertEqual(response.status_code,302)
        #authenticate session
        response = self.login("m@b.com", "FlaskApp")
        #check invalid new passwords dont match
        response = self.change_password("FlaskApp", "incorrect", "newPassword")
        self.assertEqual(response.status_code,200)
        response = json.loads(response.get_data(as_text=True))
        self.assertEqual(response['status'], 'BAD')
        user = User.query.all()[0]
        self.assertTrue(user.check_password("FlaskApp"))
        #check invalid password is incorrect
        response = self.change_password("incorrect", "newPassword", "newPassword")
        self.assertEqual(response.status_code,200)
        response = json.loads(response.get_data(as_text=True))
        self.assertEqual(response['status'], 'BAD')
        user = User.query.all()[0]
        self.assertTrue(user.check_password("FlaskApp")) 
        
    def test_valid_confirm_password(self):
        self.create_user()
        response = self.login("m@b.com", "FlaskApp")
        #check get confirm passwrod form
        response = self.app.get("/user/confirmPassword")
        with self.subTest("request should return return the confirm password form"):
            self.assertEqual(response.status_code,200)
        #check post confirm password form
        response = self.confirm_password("FlaskApp")
        with self.subTest("request should validate the password is correct"):
            self.assertEqual(response.status_code,200)
            response = json.loads(response.get_data(as_text=True))
            self.assertEqual(response['status'], 'OK')

    def test_invalid_confirm_password(self):
        self.create_user()
        #check unathenticate get password form 
        response = self.app.get("/user/confirmPassword")
        with self.subTest("unauthenticated request should be rediected"):
            self.assertEqual(response.status_code,302)
        #check incorrect password
        response = self.login("m@b.com", "FlaskApp")
        response = self.confirm_password("FlanewPassword")
        with self.subTest("incorrect password should return BAD"):
            self.assertEqual(response.status_code,200)
            response = json.loads(response.get_data(as_text=True))
            self.assertEqual(response['status'], 'BAD')

    def test_valid_edit_user_details(self):
        self.create_user()
        time = date.today()
        response = self.login("m@b.com", "FlaskApp")
        #check get user account page
        response = self.app.get("/user/account")
        with self.subTest("request should be rediected render the users account page"):
            self.assertEqual(response.status_code,200)

        #check route succesfully
        response = self.edit_user_details("x", "x", "x@x.com", "07123456789",time)
        with self.subTest("should re-render the users account page"):
            self.assertEqual(response.status_code,200)
        #check the users details have changes correctly
        with self.subTest("should change the users details"):
            user = User.query.all()[0]
            self.assertEqual(user.first_name, "x")
            self.assertEqual(user.last_name, "x")
            self.assertEqual(user.email, "x@x.com")
            self.assertEqual(user.emergency_number, "07123456789")
            self.assertEqual(user.dob.date(), time)
        
    
    def test_valid_edit_user_details(self):
        self.create_user()
        time = date.today()
        #check get user account page
        response = self.app.get("/user/account")
        with self.subTest("unauthenticated request should be rediected to index"):
            self.assertEqual(response.status_code,302)

        response = self.login("m@b.com", "FlaskApp")
        #check route succesfully handles invalid form
        response = self.edit_user_details("", "x", "x@x.com", "07123456789",time)
        with self.subTest("should re-render the users account page"):
            self.assertEqual(response.status_code,200)
        #check the users details
        with self.subTest("shouldnot change the users name"):
            user = User.query.all()[0]
            self.assertEqual(user.first_name, "milo")
        
        response = self.edit_user_details("x", "x", "x@", "07123456789",time)
        with self.subTest("should re-render the users account page"):
            self.assertEqual(response.status_code,200)
        #check the users details
        with self.subTest("shouldnot change the users name"):
            user = User.query.all()[0]
            self.assertEqual(user.email, "m@b.com")
        
        response = self.edit_user_details("x", "x", "x@c.com", "0712356789",time)
        with self.subTest("should re-render the users account page"):
            self.assertEqual(response.status_code,200)
        #check the users details
        with self.subTest("shouldnot change the users name"):
            user = User.query.all()[0]
            self.assertEqual(user.emergency_number, "07987654321")







if __name__ == "__main__":
    unittest.main()  #
